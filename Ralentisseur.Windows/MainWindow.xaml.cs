﻿using System;
using System.Windows;
using ManipulateurFFMpeg.Library;
using System.Windows.Threading;
using Ralentisseur.Windows.ViewModels;
using Ralentisseur.Windows.Services;
using System.ComponentModel;
using Ralentisseur.Common.Services;

namespace Ralentisseur.Windows
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private Properties
        MainViewModel _mainViewModel;
        IDialogService _dlg;
        #endregion

               #region Constructors
        public MainWindow()
        {
            InitializeComponent();
            _mainViewModel = this.DataContext as MainViewModel;
            _dlg = new DialogService(this);

            _mainViewModel.MainManipulateur.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e)
            {
                if (e.PropertyName == nameof(_mainViewModel.MainManipulateur.DestinationFileName) || e.PropertyName == nameof(_mainViewModel.MainManipulateur.DestinationDirectory))
                {
                    UpdateDestinationTextBlock();
                }
            };

            if (!String.IsNullOrEmpty(_mainViewModel.MainManipulateur.DestinationDirectory))
                UpdateDestinationTextBlock();
        }
        #endregion

        #region Private Methods
        //private void UpdateSourceTextBlock()
        //{
        //    string sFileName = _mainViewModel.MainManipulateur.SourceFileName;
        //    if (!String.IsNullOrEmpty(sFileName))
        //        SourceTextBlock.Text = sFileName;
        //}

        private void UpdateDestinationTextBlock()
        {
            if (!String.IsNullOrEmpty(_mainViewModel.MainManipulateur.DestinationFileName))
                DestinationTextBlock.Text = _mainViewModel.MainManipulateur.DestinationFileName;
            else if (!String.IsNullOrEmpty(_mainViewModel.MainManipulateur.DestinationDirectory))
                DestinationTextBlock.Text = _mainViewModel.MainManipulateur.DestinationDirectory;
        }
        
        private void WaitForConversionToFinish()
        {
            OngoingWindow ongoingWindow = new OngoingWindow();
            ongoingWindow.Owner = this;

            DispatcherTimer waitTimer = new DispatcherTimer();
            waitTimer.Tick += new EventHandler((s, e) =>
            {
                if (_mainViewModel.MainManipulateur.Finished)
                {
                    ongoingWindow.Hide();
                    MessageBox.Show("Conversion terminée !", "Terminé");
                    waitTimer.Stop();
                }
                else
                {
                    Console.WriteLine("Conversion en cours...");
                }
            });
            waitTimer.Interval = new TimeSpan(0, 0, 1);
            waitTimer.Start();

            ongoingWindow.ShowDialog();
        }
        #endregion

        #region Action Methods
        private void ChooseSourceButton_Click(object sender, RoutedEventArgs e)
        {
            _mainViewModel.ChooseSource(_dlg);
            //UpdateSourceTextBlock();
        }

        private void ChooseDestinationButton_Click(object sender, RoutedEventArgs e)
        {
            _mainViewModel.ChooseDestination(_dlg);
            //UpdateDestinationTextBlock();
        }

        private void GOButton_Click(object sender, RoutedEventArgs e)
        {
            if (_mainViewModel.Go(_dlg))
            {
                //UpdateDestinationTextBlock();
                WaitForConversionToFinish();
            }
        }
        #endregion
    }
}