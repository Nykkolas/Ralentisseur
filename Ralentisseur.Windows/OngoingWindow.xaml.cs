﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Ralentisseur.Windows
{
    /// <summary>
    /// Logique d'interaction pour OngoingWindow.xaml
    /// </summary>
    public partial class OngoingWindow : Window
    {
        public OngoingWindow()
        {
            InitializeComponent();
        }

        #region Actions Methods
        void OngoingWindow_Closing(object sender, CancelEventArgs e)
        {
            MessageBox.Show("Closing event called");
            e.Cancel = true;
        }
        #endregion
    }


}
