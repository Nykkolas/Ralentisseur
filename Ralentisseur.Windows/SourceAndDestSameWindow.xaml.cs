﻿using ManipulateurFFMpeg.Library;
using System.Windows;

namespace Ralentisseur.Windows
{
    /// <summary>
    /// Logique d'interaction pour SourceAndDestSameWindow.xaml
    /// </summary>
    public partial class SourceAndDestSameWindow : Window
    {
        public DestExistsActions UserAction;

        public SourceAndDestSameWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged()
        {

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void KeepButton_Click(object sender, RoutedEventArgs e)
        {
            this.UserAction = DestExistsActions.Conserver;
            this.DialogResult = true;
        }
    }
}
