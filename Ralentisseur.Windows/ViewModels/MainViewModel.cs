﻿using ManipulateurFFMpeg.Library;
using Ralentisseur.Common.ViewModels;
using Ralentisseur.Common.Services;
using System;

namespace Ralentisseur.Windows.ViewModels
{
    public class MainViewModel : CommonViewModel
    {
        #region Private Properties
        //private Manipulateur _mainManipulateur;
        #endregion

        #region Public Properties
        //public Manipulateur MainManipulateur
        //{
        //    get { return _mainManipulateur; }
        //}
        #endregion

        #region Ctor
        public MainViewModel() : base()
        {
            string destDir = Properties.Settings.Default.DestinationDirectory;

            //_mainManipulateur = new Manipulateur();

            _mainManipulateur.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName == nameof(_mainManipulateur.DestinationDirectory))
                {
                    Console.WriteLine("toit");
                    Properties.Settings.Default.DestinationDirectory = _mainManipulateur.DestinationDirectory;
                    Properties.Settings.Default.Save();
                }
                    
            };
            if (!string.IsNullOrEmpty(destDir))
                _mainManipulateur.DestinationDirectory = destDir;
        }
        #endregion

        //#region Public Methods
        //public void ChooseSource(IDialogService dlg)
        //{
        //    if (dlg.AskSource())
        //    {
        //        _mainManipulateur.SourceFileName = dlg.Result;
        //    }
        //}

        //public void ChooseDestination(IDialogService dlg)
        //{
        //    if (dlg.AskDestination())
        //    {
        //        _mainManipulateur.DestinationDirectory = dlg.Result;
        //    }

        //}

        //public bool Go(IDialogService dlg)
        //{
        //    if (_mainManipulateur.CheckConversionReady())
        //    {
        //        _mainManipulateur.StartConversion();
        //        return true;
        //    }
        //    else
        //    {
        //        switch (_mainManipulateur.ErrorStatus)
        //        {
        //            case ConversionStatus.SourceAndDestSame:
        //                if (dlg.AskSourceAndDestSame(_mainManipulateur.GetSuffixDestinationFileName()) == true && dlg.Action == DestExistsActions.Conserver)
        //                {
        //                    _mainManipulateur.StartConversionDestinationExists(dlg.Action);
        //                    return true;
        //                }
        //                return false;
        //            case ConversionStatus.DestFileAlreadyExists:
        //                if (dlg.AskDestinationAlreadyExists(_mainManipulateur.DestinationFileName, _mainManipulateur.GetSuffixDestinationFileName()) == true)
        //                {
        //                    _mainManipulateur.StartConversionDestinationExists(dlg.Action);
        //                    return true;
        //                }
        //                return false;
        //            case ConversionStatus.SourceDoesntExists:
        //                dlg.Inform("Erreur : le fichier Source n'existe pas. Choisi un autre fichier !", "La source n'existe pas");
        //                return false;
        //            case ConversionStatus.NoDestDirChosen:
        //                dlg.Inform("Tu n'as pas choisi la destination ! Choisi un répertoire de destination et recommance.", "Erreur");
        //                return false;
        //            default:
        //                return false;
        //        }
        //    }
        //}
        //#endregion
    }
}
