﻿using ManipulateurFFMpeg.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Ralentisseur.Windows
{
    /// <summary>
    /// Logique d'interaction pour ExistingFileWindow.xaml
    /// </summary>
    public partial class ExistingFileWindow : Window
    {
        public DestExistsActions UserAction;

        public ExistingFileWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged()
        {

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void KeepButton_Click(object sender, RoutedEventArgs e)
        {
            this.UserAction = DestExistsActions.Conserver;
            this.DialogResult = true;
        }

        private void ReplaceButton_Click(object sender, RoutedEventArgs e)
        {
            this.UserAction = DestExistsActions.Remplacer;
            this.DialogResult = true;

        }
    }
}
