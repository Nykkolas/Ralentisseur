﻿using ManipulateurFFMpeg.Library;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using Ralentisseur.Common.Services;
using System.IO;
using System.Windows;

namespace Ralentisseur.Windows.Services
{
    public class DialogService : IDialogService
    {
        #region Private Properties
        private Window _parent;
        private string _result;
        private DestExistsActions _action;
        #endregion

        #region Ctor
        public DialogService(Window parent)
        {
            _parent = parent;
        }
        #endregion

        #region Interface Implementation
        public string Result
        {
            get { return _result; }
        }

        public DestExistsActions Action
        {
            get { return _action; }
        }

        public bool AskSource()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            openFileDialog.Filter = "Fichiers MP4 (*.mp4)|*.mp4";
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == true)
            {
                _result = openFileDialog.FileName;
                return true;              
            }

            return false;
        }

        public bool AskDestination (string currentDest = "")
        {
            var openFolderDialog = new CommonOpenFileDialog();
            openFolderDialog.Title = "Choisir le répertoire de destination";
            openFolderDialog.IsFolderPicker = true;
            openFolderDialog.InitialDirectory = currentDest;
            openFolderDialog.AddToMostRecentlyUsedList = false;
            openFolderDialog.EnsureFileExists = true;
            openFolderDialog.EnsurePathExists = true;
            openFolderDialog.EnsureValidNames = true;
            openFolderDialog.Multiselect = false;
            openFolderDialog.ShowPlacesList = true;
            openFolderDialog.RestoreDirectory = true;

            if (openFolderDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                _result = openFolderDialog.FileName;
                return true;
            }

            return false;
        }

        public bool AskDestination()
        {
            var openFolderDialog = new CommonOpenFileDialog();
            openFolderDialog.Title = "Choisir le répertoire de destination";
            openFolderDialog.IsFolderPicker = true;
            //openFolderDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            openFolderDialog.AddToMostRecentlyUsedList = false;
            openFolderDialog.EnsureFileExists = true;
            openFolderDialog.EnsurePathExists = true;
            openFolderDialog.EnsureValidNames = true;
            openFolderDialog.Multiselect = false;
            openFolderDialog.ShowPlacesList = true;
            openFolderDialog.RestoreDirectory = true;

            if (openFolderDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                _result = openFolderDialog.FileName;
                return true;
            }

            return false;
        }

        public bool AskSourceAndDestSame(string filename)
        {
            SourceAndDestSameWindow dlg = new SourceAndDestSameWindow();
            dlg.Owner = _parent;
            dlg.TitleLabel.Content = "Les fichiers source et destination sont les mêmes !";
            dlg.MessageLabel.Content = "Le fichier que tu as choisi comme destination est le même que le fichier source.\n\n" +
    "Tu peux :\n" +
    "- Cliquer sur Annuler pour annuler la conversion et revenir à la fenêtre précédente\n" +
    "- Cliquer sur Continuer pour créer un nouveau fichier qui se nommera \"" +
    System.IO.Path.GetFileName(filename) + "\"\n\n" +
    "Quelque soit ton choix, le fichier source sera conservé.";
            if (dlg.ShowDialog() == true)
            {
                _action = dlg.UserAction;
                return true;
            }
            return false;
        }

        public bool AskDestinationAlreadyExists(string filename, string keepFilename)
        {
            ExistingFileWindow dlg = new ExistingFileWindow();
            dlg.Owner = _parent;
            dlg.TitleLabel.Content = "Le fichier " + Path.GetFileName(filename) + " éxiste déjà !";
            dlg.MessageLabel.Content = "Le fichier que tu as choisi comme destination existe déjà.\n\n" +
    "Tu peux :\n" +
    "- Cliquer sur Annuler pour annuler la conversion et revenir à la fenêtre précédente\n" +
    "- Cliquer sur Conserver pour créer un nouveau fichier qui se nommera \"" +
    Path.GetFileName(keepFilename) + "\"\n" +
    "- Cliquer sur Remplacer pour l'effacer et le remplacer\n";
            if (dlg.ShowDialog() == true)
            {
                _action = dlg.UserAction;
                return true;
            }
            return false;
        }

        public bool Inform(string information, string titre)
        {
            MessageBox.Show(_parent, information, titre);
            return true;
        }
        #endregion
    }
}
