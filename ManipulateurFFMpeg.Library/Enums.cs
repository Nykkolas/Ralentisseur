﻿using System;
namespace ManipulateurFFMpeg.Library
{
    public enum DestExistsActions
    {
        Conserver = 0,
        Remplacer = 1,
        Annuler = 3
    }

    public enum ConversionStatus
    {
        Ok = 0,
        DestFileAlreadyExists = 1,
        DestIsDirectory = 2,
        SourceDoesntExists = 3,
        SourceAndDestSame = 4,
        NoDestDirChosen = 5
    }
}
