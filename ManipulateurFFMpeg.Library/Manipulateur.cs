﻿using System;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using Shared.Base;
using Xabe.FFmpeg;
using Xabe.FFmpeg.Enums;

namespace ManipulateurFFMpeg.Library
{
    public class Manipulateur : BaseNotifyPropertyChanged
    {
        #region Private Const Properties
        private const double _targetFramerate = 18;
        #endregion

        #region Private Properties
        private bool _finished = false;
        private string _sourceFileName = "";
        private string _destinationDirectory = "";
        private string _destinationFileName = "";
        private ConversionStatus _errorStatus = ConversionStatus.Ok;
        private MediaInfo _sourceVideoInfo;
        #endregion

        #region Computed Properties
        public bool Finished
        {
            get { return _finished; }
            //set { _finished = value; }
        }
        public string SourceFileName
        {
            get { return _sourceFileName; }
            set
            {
                SetProperty(ref _sourceFileName, value);
                this._sourceVideoInfo = new MediaInfo(_sourceFileName);
                if (Directory.Exists(_destinationDirectory))
                    SetDestFileName();
            }
        }

        public string DestinationDirectory
        {
            get { return _destinationDirectory; }
            set {
                SetProperty(ref _destinationDirectory, value);

                if (File.Exists(_sourceFileName))
                    SetDestFileName();
            }
        }

        public string DestinationFileName
        {
            get { return _destinationFileName; }
            //set { this._destinationFileName = value; } //Passer uniquement par DestinationDirectory
        }
        public ConversionStatus ErrorStatus
        {
            get { return _errorStatus; }
        }
        public MediaInfo SourceVideoInfo
        {
            get { return _sourceVideoInfo; }
        }
        #endregion

        #region Constructors
        public Manipulateur()
        {
            FFbase.FFmpegDir = Ressources.BinDir;
        }

        public Manipulateur(string filename) : this()
        {
            this._sourceFileName = filename;
            this._sourceVideoInfo = new MediaInfo(_sourceFileName);
        }

        public Manipulateur(string movieFileName, string destinationFileName) : this(movieFileName)
        {
            if (Directory.Exists(destinationFileName))
                DestinationDirectory = destinationFileName;
            else
            {
                this._destinationFileName = destinationFileName;
                this._destinationDirectory = Path.GetDirectoryName(destinationFileName);
            }
                
            
        }
        #endregion

        //#region Events
        //public event PropertyChangedEventHandler PropertyChanged;
        //#endregion

        #region Private Methods
        private void SetDestFileName()
        {
            SetDestFileName(Path.Combine(_destinationDirectory, Path.GetFileName(_sourceFileName)));
            //OnPropertyChanged("DestinationFileName");
        }

        private void SetDestFileName (string filename)
        {
            SetProperty(ref _destinationFileName, filename, nameof(DestinationFileName));
            //_destinationFileName = filename;
            //OnPropertyChanged("DestinationFileName");
        }

        private void ResetStatus()
        {
            SetStatus(ConversionStatus.Ok, false);
        }

        private void SetErrorStatus(ConversionStatus errorStatus)
        {
            SetStatus(errorStatus, true);
        }

        private void SetStatus(ConversionStatus errorStatus, bool finishedState)
        {
            _errorStatus = errorStatus;
            _finished = finishedState;
        }
        #endregion

        #region Public Methods
        public double getFrameRate()
        {
            if (String.IsNullOrEmpty(_sourceFileName))
                return -1;

            return _sourceVideoInfo.Properties.FrameRate;
        }

        public double getSlowdownRatio()
        {
            if (String.IsNullOrEmpty(_sourceFileName))
                return -1;

            return _targetFramerate / getFrameRate();
        }

        public string GetSuffixDestinationFileName()
        {
            string destinationFilnameWithoutExtension = Path.GetFileNameWithoutExtension(DestinationFileName);
            string destinationExtension = Path.GetExtension(DestinationFileName);
            string destinationDirectory = Path.GetDirectoryName(DestinationFileName);
            return Path.Combine(
                destinationDirectory,
                destinationFilnameWithoutExtension + " (nouveau)" + destinationExtension);
        }

        public async Task StartConversion()
        {
            if (!CheckConversionReady())
            {
                switch (_errorStatus)
                {
                    case ConversionStatus.DestIsDirectory:
                        SetDestFileName();
                        if (CheckConversionReady())
                            break;
                        else
                            return;
                    default:
                        return;
                }

            }
                
            ResetStatus();
            _finished = await new Conversion()
                .SetInput(_sourceFileName)
                .SetOutput(_destinationFileName)
                .ChangeSpeed(Channel.Both, getSlowdownRatio())
                .Start();
        }

        public bool StartConversionDestinationExists(DestExistsActions action)
        {
             switch (action)
            {
                case DestExistsActions.Remplacer:
                    File.Delete(_destinationFileName);
                    StartConversion();
                    break;
                case DestExistsActions.Conserver:
                    SetDestFileName(GetSuffixDestinationFileName());
                    StartConversion();
                    break;
                default:
                    return false;
            }
            return true;
        }

        public bool CheckConversionReady()
        {
            if (!File.Exists(_sourceFileName))
            {
                SetErrorStatus(ConversionStatus.SourceDoesntExists);
                return false;
            }

            if (!Directory.Exists(_destinationDirectory))
            {
                SetErrorStatus(ConversionStatus.NoDestDirChosen);
                return false;
            }

            if (Directory.Exists(_destinationFileName))
            {
                SetErrorStatus(ConversionStatus.DestIsDirectory);
                return false;
            }

            if (File.Exists(_destinationFileName))
            {
                if (_sourceFileName == _destinationFileName)
                    SetErrorStatus(ConversionStatus.SourceAndDestSame);
                else
                    SetErrorStatus(ConversionStatus.DestFileAlreadyExists);
                return false;
            }

            return true;
        }
        #endregion
    }
}
