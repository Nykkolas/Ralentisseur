﻿using System;
using System.IO;
using System.Reflection;

namespace ManipulateurFFMpeg.Library
{
    internal static class Ressources
    {
        internal static readonly string BinDir = Path.Combine(
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), 
            "Ressources", 
            "bin", 
            RunningPlatform());

        private static string RunningPlatform()
        {
            // Source : https://stackoverflow.com/questions/10138040/how-to-detect-properly-windows-linux-mac-operating-systems
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                    // Well, there are chances MacOSX is reported as Unix instead of MacOSX.
                    // Instead of platform check, we'll do a feature checks (Mac specific root folders)
                    if (Directory.Exists("/Applications")
                        & Directory.Exists("/System")
                        & Directory.Exists("/Users")
                        & Directory.Exists("/Volumes"))
                        return "Mac";
                    else
                        return "Linux";

                case PlatformID.MacOSX:
                    return "Mac";

                default:
                    return "Windows";
            }
        }
    }
}
