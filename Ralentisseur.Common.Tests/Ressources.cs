﻿using System;
using System.IO;
using System.Reflection;

namespace Ralentisseur.Common.Tests
{
    internal static class Ressources
    {
        internal static readonly string UneSecondeFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Ressources", "TestFiles", "UneSeconde.mp4");
        internal static readonly string FichierTest = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Ressources", "TestFiles", "Fichier test.mp4");
        internal static readonly string OutputDir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Ressources", "TestFiles", "Output");
        internal static readonly string SameOutputDir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Ressources", "TestFiles", "SameOutput");
        internal static readonly string DestDir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Ressources", "TestFiles", "Output");
    }
}
