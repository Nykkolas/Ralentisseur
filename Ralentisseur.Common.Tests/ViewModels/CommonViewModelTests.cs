﻿//using NUnit.Framework;
//using Ralentisseur.Common.ViewModels;
//using Moq;
//using Ralentisseur.Common.Services;
//using System.IO;
//using ManipulateurFFMpeg.Library;
//using System.Threading;
//using System;
//using System.ComponentModel;

//namespace Ralentisseur.Common.Tests
//{
//    [TestFixture]
//    public class MainViewModelTests
//    {
//        #region Public Properties
//        public CommonViewModel Mc;
//        #endregion

//        #region Setup
//        [SetUp()]
//        public void SetUp()
//        {
//            Mc = new CommonViewModel();
//        }

//        private Mock<IDialogService> SetupSourceDialogMock(bool rValue, string result)
//        {
//            var dlgMock = new Mock<IDialogService>();
//            dlgMock.Setup(d => d.AskSource())
//                .Returns(rValue);
//            dlgMock.Setup(d => d.Result)
//                   .Returns(result);

//            return dlgMock;
//        }

//        private void SetUpValidSource(Mock<IDialogService> dlgMock, string sourceFileName)
//        {
//            dlgMock.Setup(d => d.AskSource())
//                   .Returns(true);
//            dlgMock.Setup(d => d.Result)
//                   .Returns(sourceFileName);
//        }

//        private IDialogService Setup_Go_WhenDestinationAlreadyExists(Mock<IDialogService> dlgMock)
//        {
//            SetUpValidSource(dlgMock, Ressources.UneSecondeFile);
//            IDialogService dlg = dlgMock.Object;
//            Mc.ChooseSource(dlg);
//            dlgMock.Setup(d => d.AskDestination())
//                   .Returns(true);
//            dlgMock.Setup(d => d.Result)
//                   .Returns(Ressources.SameOutputDir);
//            Mc.ChooseDestination(dlg);

//            return dlg;
//        }

//        private IDialogService Setup_Go_WhenSourceAndDestinationSame(Mock<IDialogService> dlgMock)
//        {
//            SetUpValidSource(dlgMock, Ressources.UneSecondeFile);
//            IDialogService dlg = dlgMock.Object;
//            Mc.ChooseSource(dlg);

//            dlgMock.Setup(d => d.AskDestination())
//                   .Returns(true);
//            dlgMock.Setup(d => d.Result)
//                   .Returns(Path.GetDirectoryName(Ressources.UneSecondeFile));
//            Mc.ChooseDestination(dlg);

//            return dlg;
//        }

//        #endregion

//        #region Ctor Tests
//        [Test]
//        public void Ctor_WhenCalled_ThenBuildWithManipulateur()
//        {
//            //Arrange

//            //Act

//            //Assert
//            Assert.NotNull(Mc);
//            Assert.IsEmpty(Mc.MainManipulateur.SourceFileName);
//        }
//        #endregion

//        #region ChooseSource Tests
//        [Test]
//        public void ChooseSource_WhenUserCancel_ThenNothing()
//        {
//            //Arrange
//            var dlgMock = SetupSourceDialogMock(false, "");
//            IDialogService dlg = dlgMock.Object;

//            //Act
//            Mc.ChooseSource(dlg);

//            //Assert
//            dlgMock.Verify(dialog => dialog.AskSource());
//            Assert.IsEmpty(Mc.MainManipulateur.SourceFileName);
//        }

//        [Test]
//        public void ChooseSource_WhenSelectedFile_ThenUpdateManipulateurProperty()
//        {
//            //Arrange
//            var dlgMock = SetupSourceDialogMock(true, Ressources.UneSecondeFile);
//            IDialogService dlg = dlgMock.Object;

//            //Act
//            Mc.ChooseSource(dlg);

//            //Assert
//            dlgMock.Verify(dialog => dialog.AskSource());
//            Assert.AreEqual(Ressources.UneSecondeFile, Mc.MainManipulateur.SourceFileName);
//        }

//        [Test]
//        public void ChooseSource_WhenSourceChosen_ThenEventIsRaised()
//        {
//            //Arrange
//            var dlgMock = SetupSourceDialogMock(true, Ressources.UneSecondeFile);
//            IDialogService dlg = dlgMock.Object;
//            string actualName = null;

//            //Act
//            Mc.MainManipulateur.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e)
//            {
//                actualName = e.PropertyName;
//            };
//            Mc.ChooseSource(dlg);

//            //Assert
//            dlgMock.Verify(dialog => dialog.AskSource());
//            Assert.AreEqual(Ressources.UneSecondeFile, Mc.MainManipulateur.SourceFileName);
//            Assert.AreEqual("SourceFileName", actualName);
//        }
//        #endregion

//        #region ChooseDestination Tests
//        [Test]
//        public void ChooseDestination_WhenCancel_ThenNothing()
//        {
//            //Arange
//            var dlgMock = new Mock<IDialogService>();
//            dlgMock.Setup(d => d.AskDestination())
//                   .Returns(false);
//            IDialogService dlg = dlgMock.Object;

//            //Act
//            Mc.ChooseDestination(dlg);

//            //Assert
//            dlgMock.Verify(d => d.AskDestination());
//            Assert.IsEmpty(Mc.MainManipulateur.DestinationDirectory);
//        }

//        [Test]
//        public void ChooseDestination_WhenSelectedDirectory_ThenUpdateManipulateurProperty()
//        {
//            //Arange
//            var dlgMock = new Mock<IDialogService>();
//            dlgMock.Setup(d => d.AskDestination())
//                   .Returns(true);
//            dlgMock.Setup(d => d.Result)
//                   .Returns(Ressources.OutputDir);
//            IDialogService dlg = dlgMock.Object;

//            //Act
//            Mc.ChooseDestination(dlg);

//            //Assert
//            dlgMock.Verify(d => d.AskDestination());
//            Assert.AreEqual(Ressources.OutputDir, Mc.MainManipulateur.DestinationDirectory);
//        }

//        [Test]
//        public void ChooseDestination_WhenSourceFileChanged_ThenEventIsRaised()
//        {
//            //Arrange
//            var dlgMock = new Mock<IDialogService>();
//            IDialogService dlg = dlgMock.Object;
//            dlgMock.Setup(d => d.AskSource())
//                   .Returns(true);
//            dlgMock.Setup(d => d.Result)
//                   .Returns(Ressources.UneSecondeFile);
//            Mc.ChooseSource(dlg);
//            dlgMock.Setup(d => d.AskDestination())
//                   .Returns(true);
//            dlgMock.Setup(d => d.Result)
//                   .Returns(Ressources.OutputDir);
//            Mc.ChooseDestination(dlg);
//            dlgMock.Setup(d => d.Result)
//                   .Returns(Ressources.FichierTest);

//            bool seen = false;

//            //Act
//            Mc.MainManipulateur.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e)
//            {
//                if (e.PropertyName == "DestinationFileName")
//                    seen = true;
//            };
//            Mc.ChooseSource(dlg);

//            //Assert
//            Assert.AreEqual(Path.Combine(Ressources.OutputDir, Path.GetFileName(Ressources.FichierTest)), Mc.MainManipulateur.DestinationFileName);
//            Assert.IsTrue(seen);
//        }
//        #endregion

//        #region Go Tests
//        [Test]
//        public void Go_WhenSourceEmpty_ThenAlertUser()
//        {
//            //Arrange
//            var dlgMock = new Mock<IDialogService>();
//            dlgMock.Setup(d => d.Inform("Merci de choisir un fichier source et un répertoire destination", ""));
//            IDialogService dlg = dlgMock.Object;

//            //Act
//            bool result = Mc.Go(dlg);

//            //Assert
//            dlgMock.Verify(d => d.Inform("Merci de choisir un fichier source et un répertoire destination", ""));
//            Assert.False(result);
//        }

//        [Test]
//        public void Go_WhenDestinationDirectoryEmpty_ThenAlertUser()
//        {
//            //Arrange
//            var dlgMock = new Mock<IDialogService>();
//            dlgMock.Setup(d => d.Inform("Merci de choisir un fichier source et un répertoire destination", ""));
//            dlgMock.Setup(d => d.AskSource())
//                   .Returns(true);
//            dlgMock.Setup(d => d.Result)
//                   .Returns(Ressources.UneSecondeFile);
//            IDialogService dlg = dlgMock.Object;
//            Mc.ChooseSource(dlg);

//            //Act
//            bool result = Mc.Go(dlg);

//            //Assert
//            dlgMock.Verify(d => d.Inform("Merci de choisir un fichier source et un répertoire destination", ""));
//            Assert.AreEqual(Ressources.UneSecondeFile, Mc.MainManipulateur.SourceFileName);
//            Assert.False(result);
//        }

//        [Test]
//        public void Go_WhenDestinationAlreadyExists_ThenAskWhatToDoCancel()
//        {
//            //Arrange
//            var dlgMock = new Mock<IDialogService>();
//            var dlg = Setup_Go_WhenDestinationAlreadyExists(dlgMock);

//            dlgMock.Setup(d => d.AskDestinationAlreadyExists(Mc.MainManipulateur.DestinationFileName, Mc.MainManipulateur.GetSuffixDestinationFileName()))
//                   .Returns(false);
//            dlgMock.Setup(d => d.Action)
//                   .Returns(DestExistsActions.Annuler);

//            //Act
//            bool result = Mc.Go(dlg);

//            //Assert
//            dlgMock.Verify(d => d.AskDestinationAlreadyExists(Mc.MainManipulateur.DestinationFileName, Mc.MainManipulateur.GetSuffixDestinationFileName()));
//            Assert.False(result);
//        }

//        [Test]
//        public void Go_WhenDestinationAlreadyExistsAndAnswerKeep_ThenKeep()
//        {
//            var dlgMock = new Mock<IDialogService>();
//            var dlg = Setup_Go_WhenDestinationAlreadyExists(dlgMock);

//            string destfile = Mc.MainManipulateur.DestinationFileName;
//            string destFileNew = Mc.MainManipulateur.GetSuffixDestinationFileName();
//            dlgMock.Setup(d => d.AskDestinationAlreadyExists(destfile, destFileNew))
//                   .Returns(true);
//            dlgMock.Setup(d => d.Action)
//                   .Returns(DestExistsActions.Conserver);

//            //Act
//            bool result = Mc.Go(dlg);

//            //Assert
//            dlgMock.Verify(d => d.AskDestinationAlreadyExists(destfile, destFileNew));
//            Assert.True(result);
//            while (!Mc.MainManipulateur.Finished)
//                Thread.Sleep(5);
//            Assert.True(File.Exists(Mc.MainManipulateur.DestinationFileName));
//            File.Delete(Mc.MainManipulateur.DestinationFileName);
//        }

//        [Test]
//        public void Go_DestinationAlreadyExists_ThenAskWhatToDoReplace()
//        {
//            //Arrange
//            var dlgMock = new Mock<IDialogService>();
//            var dlg = Setup_Go_WhenDestinationAlreadyExists(dlgMock);

//            dlgMock.Setup(d => d.AskDestinationAlreadyExists(Mc.MainManipulateur.DestinationFileName, Mc.MainManipulateur.GetSuffixDestinationFileName()))
//                   .Returns(true);
//            dlgMock.Setup(d => d.Action)
//                   .Returns(DestExistsActions.Remplacer);

//            //Act
//            DateTime destWriteTime = File.GetLastWriteTime(Mc.MainManipulateur.DestinationFileName);
//            bool result = Mc.Go(dlg);

//            //Assert
//            dlgMock.Verify(d => d.AskDestinationAlreadyExists(Mc.MainManipulateur.DestinationFileName, Mc.MainManipulateur.GetSuffixDestinationFileName()));
//            Assert.True(result);
//            while (!Mc.MainManipulateur.Finished)
//                Thread.Sleep(5);
//            Assert.Less(destWriteTime, File.GetLastWriteTime(Mc.MainManipulateur.DestinationFileName));
//        }

//        [Test]
//        public void Go_WhenSourceAndDestinationSame_ThenAskWhatToDoCancel()
//        {
//            //Arrange
//            var dlgMock = new Mock<IDialogService>();
//            IDialogService dlg = Setup_Go_WhenSourceAndDestinationSame(dlgMock);
//            string keepFilename = Mc.MainManipulateur.GetSuffixDestinationFileName();

//            dlgMock.Setup(d => d.AskSourceAndDestSame(keepFilename))
//                   .Returns(false);
//            dlgMock.Setup(d => d.Action)
//                   .Returns(DestExistsActions.Annuler);

//            //Act
//            bool result = Mc.Go(dlg);

//            //Assert
//            dlgMock.Verify(d => d.AskSourceAndDestSame(keepFilename));
//            Assert.False(result);
//        }

//        [Test]
//        public void Go_WhenSourceAndDestinationSame_ThenAskWhatToDoKeep()
//        {
//            var dlgMock = new Mock<IDialogService>();
//            IDialogService dlg = Setup_Go_WhenSourceAndDestinationSame(dlgMock);

//            string destFilename = Mc.MainManipulateur.DestinationFileName;
//            string keepFilename = Mc.MainManipulateur.GetSuffixDestinationFileName();

//            dlgMock.Setup(d => d.AskSourceAndDestSame(keepFilename))
//                   .Returns(true);
//            dlgMock.Setup(d => d.Action)
//                   .Returns(DestExistsActions.Conserver);

//            //Act
//            DateTime destTime = File.GetLastWriteTime(destFilename);
//            bool result = Mc.Go(dlg);

//            //Assert
//            dlgMock.Verify(d => d.AskSourceAndDestSame(keepFilename));
//            Assert.True(result);
//            while (!Mc.MainManipulateur.Finished)
//                Thread.Sleep(5);
//            Assert.True(File.Exists(Mc.MainManipulateur.DestinationFileName));
//            Assert.AreEqual(destTime, File.GetLastWriteTime(destFilename));
//            File.Delete(Mc.MainManipulateur.DestinationFileName);
//        }

//        [Test]
//        public void Go_WhenEverythingIsOk_ThenConvert()
//        {
//            //Arrange
//            var dlgMock = new Mock<IDialogService>();
//            SetUpValidSource(dlgMock, Ressources.UneSecondeFile);
//            IDialogService dlg = dlgMock.Object;
//            Mc.ChooseSource(dlg);

//            dlgMock.Setup(d => d.AskDestination())
//                   .Returns(true);
//            dlgMock.Setup(d => d.Result)
//                   .Returns(Ressources.OutputDir);
//            Mc.ChooseDestination(dlg);

//            //Act
//            bool result = Mc.Go(dlg);

//            //Assert
//            Assert.True(result);
//            while (!Mc.MainManipulateur.Finished)
//                Thread.Sleep(5);
//            Assert.True(File.Exists(Mc.MainManipulateur.DestinationFileName));
//            File.Delete(Mc.MainManipulateur.DestinationFileName);
//        }

//        //[Test]
//        //public void Go_WhenDestinationDirectoryEmpty_ThenInform()
//        //{
//        //    //Arrange
//        //    Mock<IDialogService> dlgMock = new Mock<IDialogService>();
//        //    IDialogService dlg = dlgMock.Object;
//        //    Vm.MainManipulateur.DestinationDirectory = "";
//        //    SetupValidSource(dlgMock, dlg, Ressources.UneSecondeFile);
//        //    dlgMock.Setup(d => d.Inform("Tu n'as pas choisi la destination ! Choisi un répertoire de destination et recommance.", "Erreur"))
//        //        .Returns(true);

//        //    //Act
//        //    bool result = Vm.Go(dlg);

//        //    //Assert
//        //    Assert.IsFalse(result);
//        //    dlgMock.Verify(d => d.Inform("Tu n'as pas choisi la destination ! Choisi un répertoire de destination et recommance.", "Erreur"));
//        //}
//        #endregion
//    }
//}

using System;
using System.IO;
using System.Threading;
using Moq;
using NUnit.Framework;
using Ralentisseur.Common.Services;
using Ralentisseur.Common.ViewModels;
using ManipulateurFFMpeg.Library;
using System.Configuration;

namespace Ralentisseur.Common.Tests
{
    [TestFixture]
    public class MainViewModelTests
    {
        #region Properties
        public CommonViewModel Vm;
        public Mock<IDialogService> DlgMock;
        IDialogService Dlg;
        #endregion

        #region SetUp
        [SetUp]
        public void SetUp()
        {
            Vm = new CommonViewModel();
            DlgMock = new Mock<IDialogService>();
            Dlg = DlgMock.Object;
        }

        private void SetupValidSource(string source)
        {
            DlgMock.Setup(d => d.AskSource())
                .Returns(true);
            DlgMock.Setup(d => d.Result)
                .Returns(source);
            Vm.ChooseSource(Dlg);
        }

        
        private void SetupValidDestination(string destDir, string previousDestDir = "")
        {
            SetupValidDestinationMock(destDir, previousDestDir);
            Vm.ChooseDestination(Dlg);
        }

        private void SetupValidDestinationMock(string destDir, string previousDestDir = "")
        {
            DlgMock.Setup(d => d.AskDestination(previousDestDir))
                    .Returns(true);
            DlgMock.Setup(d => d.Result)
                .Returns(destDir);
        }
        #endregion

        #region Ctor
        [Test]
        public void Ctor()
        {
            //Arrange

            //Act

            //Assert
            Assert.IsNotNull(Vm);
            Assert.IsNotNull(Vm.MainManipulateur);
        }
        #endregion

        #region ChooseSource
        [Test]
        public void ChooseSource_WhenCancel_ThenNothing()
        {
            //Arrange
            var dlgMock = new Mock<IDialogService>();
            dlgMock.Setup(d => d.AskSource())
                .Returns(false);
            IDialogService dlg = dlgMock.Object;
            string currentSource = Vm.MainManipulateur.SourceFileName;

            //Act
            Vm.ChooseSource(dlg);

            //Assert
            dlgMock.Verify(d => d.AskSource());
            Assert.AreEqual(currentSource, Vm.MainManipulateur.SourceFileName);
        }

        [Test]
        public void ChooseSource_WhenFileSelected_ThenChangeSource()
        {
            //Arrange
            var dlgMock = new Mock<IDialogService>();
            dlgMock.Setup(d => d.AskSource())
                .Returns(true);
            dlgMock.Setup(d => d.Result)
                .Returns(Ressources.UneSecondeFile);
            IDialogService dlg = dlgMock.Object;

            //Act
            Vm.ChooseSource(dlg);

            //Assert
            dlgMock.Verify(d => d.AskSource());
            dlgMock.Verify(d => d.Result);
            Assert.AreEqual(Ressources.UneSecondeFile, Vm.MainManipulateur.SourceFileName);
        }
        #endregion

        #region ChooseDestination
        [Test]
        public void ChooseDestination_WhenCancel_ThenNothing()
        {
            //Arrange
            DlgMock.Setup(d => d.AskDestination(It.IsAny<string>()))
                .Returns(false);
            string currentDestDir = Vm.MainManipulateur.DestinationDirectory;
            string currentDestFile = Vm.MainManipulateur.DestinationFileName;

            //Act
            Vm.ChooseDestination(Dlg);

            //Assert
            DlgMock.VerifyAll();
            Assert.AreEqual(currentDestDir, Vm.MainManipulateur.DestinationDirectory);
            Assert.AreEqual(currentDestFile, Vm.MainManipulateur.DestinationFileName);
        }

        [Test]
        public void ChooseDestination_WhenSelectedDirectoryFirst_ThenDestnationSet()
        {
            //Arrange
            SetupValidDestinationMock(Ressources.DestDir);
            
            string destFile = Vm.MainManipulateur.DestinationFileName;

            //Act
            Vm.ChooseDestination(Dlg);

            //Assert
            DlgMock.VerifyAll();
            Assert.AreEqual(Ressources.DestDir, Vm.MainManipulateur.DestinationDirectory);
            Assert.AreEqual(destFile, Vm.MainManipulateur.DestinationFileName);
        }

        [Test]
        public void ChooseDestination_WhenSelectedDirectorySecond_ThenDestnationSet()
        {
            //Arrange
            SetupValidSource(Ressources.UneSecondeFile);
            SetupValidDestinationMock(Ressources.DestDir);

            string destFile = Path.Combine(Ressources.DestDir, Path.GetFileName(Ressources.UneSecondeFile));

            //Act
            Vm.ChooseDestination(Dlg);

            //Assert
            DlgMock.VerifyAll();
            Assert.AreEqual(Ressources.DestDir, Vm.MainManipulateur.DestinationDirectory);
            Assert.AreEqual(destFile, Vm.MainManipulateur.DestinationFileName);
        }

        [Test]
        public void ChooseDestination_WhenADestinationAsAlreadyBeenChosen_ThenPassItToDialog()
        {
            //Arrange
            SetupValidDestination(Ressources.DestDir);
            SetupValidSource(Ressources.FichierTest);

            //Act
            SetupValidDestination(Ressources.SameOutputDir, Ressources.DestDir);

            //Assert
            DlgMock.Verify(d => d.AskDestination(Ressources.DestDir));
        }
        #endregion

        #region Go Tests
        [Test]
        public void Go_WhenSourceAndDestination_ThenConvert()
        {
            //Arrange
            SetupValidSource(Ressources.UneSecondeFile);
            SetupValidDestination(Ressources.DestDir);

            //Act
            bool result = Vm.Go(Dlg);

            //Assert
            Assert.IsTrue(result);
            while (!Vm.MainManipulateur.Finished)
                Thread.Sleep(5);
            Assert.IsTrue(File.Exists(Vm.MainManipulateur.DestinationFileName));
            File.Delete(Vm.MainManipulateur.DestinationFileName);
        }

        [Test]
        public void Go_WhenSourceAndDestinationFileSame_ThenAskAnswerCancel()
        {
            //Arrange
            SetupValidSource(Ressources.UneSecondeFile);
            SetupValidDestination(Path.GetDirectoryName(Ressources.UneSecondeFile));
            DateTime sourceTime = File.GetLastWriteTime(Ressources.UneSecondeFile);

            DlgMock.Setup(d => d.AskSourceAndDestSame(Vm.MainManipulateur.GetSuffixDestinationFileName()))
                .Returns(false);

            //Act
            bool result = Vm.Go(Dlg);

            //Assert
            Assert.IsFalse(result);
            DlgMock.Verify(d => d.AskSourceAndDestSame(Vm.MainManipulateur.GetSuffixDestinationFileName()));
            Assert.AreEqual(sourceTime, File.GetLastWriteTime(Ressources.UneSecondeFile));
        }

        [Test]
        public void Go_WhenSourceAndDestinationFileSame_ThenAskAnswerKeep()
        {
            //Arrange
            string sourcefile = Ressources.UneSecondeFile;
            string destDir = Path.GetDirectoryName(sourcefile);
            SetupValidSource(sourcefile);
            SetupValidDestination(destDir);
            DateTime sourceTime = File.GetLastWriteTime(sourcefile);

            string expectedDestFile = Vm.MainManipulateur.GetSuffixDestinationFileName();
            DlgMock.Setup(d => d.AskSourceAndDestSame(expectedDestFile))
                .Returns(true);
            DlgMock.Setup(d => d.Action)
                .Returns(DestExistsActions.Conserver);

            //Act
            bool result = Vm.Go(Dlg);

            //Assert
            Assert.IsTrue(result);
            DlgMock.Verify(d => d.AskSourceAndDestSame(expectedDestFile));
            DlgMock.Verify(d => d.Action);
            while (!Vm.MainManipulateur.Finished)
                Thread.Sleep(5);
            Assert.AreEqual(sourceTime, File.GetLastWriteTime(sourcefile));
            Assert.IsTrue(File.Exists(Vm.MainManipulateur.DestinationFileName));
            File.Delete(Vm.MainManipulateur.DestinationFileName);
        }

        [Test]
        public void Go_WhenDestinationAlreadyExists_ThenAskAnswerKeep()
        {
            //Arrange
            string sourcefile = Ressources.UneSecondeFile;
            string destdir = Ressources.SameOutputDir;
            SetupValidSource(sourcefile);
            SetupValidDestination(destdir);
            DateTime sourceTime = File.GetLastWriteTime(sourcefile);

            string destFilename = Vm.MainManipulateur.DestinationFileName;
            string keepFilename = Vm.MainManipulateur.GetSuffixDestinationFileName();
            DlgMock.Setup(d => d.AskDestinationAlreadyExists(destFilename, keepFilename))
                .Returns(true);
            DlgMock.Setup(d => d.Action)
                .Returns(DestExistsActions.Conserver);

            //Act
            bool result = Vm.Go(Dlg);

            //Assert
            Assert.IsTrue(result);
            DlgMock.Verify(d => d.AskDestinationAlreadyExists(destFilename, keepFilename));
            DlgMock.Verify(d => d.Action);
            while (!Vm.MainManipulateur.Finished)
                Thread.Sleep(5);
            Assert.AreEqual(sourceTime, File.GetLastWriteTime(sourcefile));
            Assert.IsTrue(File.Exists(keepFilename));
            File.Delete(Vm.MainManipulateur.DestinationFileName);
        }

        [Test]
        public void Go_WhenDestinationAlreadyExists_ThenAskAnswerCancel()
        {
            //Arrange
            string sourcefile = Ressources.UneSecondeFile;
            string destdir = Ressources.SameOutputDir;
            SetupValidSource(sourcefile);
            SetupValidDestination(destdir);
            DateTime sourceTime = File.GetLastWriteTime(sourcefile);

            string destFilename = Vm.MainManipulateur.DestinationFileName;
            string keepFilename = Vm.MainManipulateur.GetSuffixDestinationFileName();
            DlgMock.Setup(d => d.AskDestinationAlreadyExists(destFilename, keepFilename))
                .Returns(false);

            //Act
            bool result = Vm.Go(Dlg);

            //Assert
            Assert.IsFalse(result);
            DlgMock.Verify(d => d.AskDestinationAlreadyExists(destFilename, keepFilename));
            Assert.AreEqual(sourceTime, File.GetLastWriteTime(sourcefile));
        }

        [Test]
        public void Go_WhenDestinationAlreadyExistsAnswerReplace_ThenReplace()
        {
            //Arrange
            string sourcefile = Ressources.UneSecondeFile;
            string destdir = Ressources.SameOutputDir;
            SetupValidSource(sourcefile);
            SetupValidDestination(destdir);
            DateTime sourceTime = File.GetLastWriteTime(sourcefile);

            string destFilename = Vm.MainManipulateur.DestinationFileName;
            string keepFilename = Vm.MainManipulateur.GetSuffixDestinationFileName();
            DlgMock.Setup(d => d.AskDestinationAlreadyExists(destFilename, keepFilename))
                .Returns(true);
            DlgMock.Setup(d => d.Action)
                .Returns(DestExistsActions.Remplacer);

            //Act
            bool result = Vm.Go(Dlg);

            //Assert
            Assert.IsTrue(result);
            DlgMock.Verify(d => d.AskDestinationAlreadyExists(destFilename, keepFilename));
            DlgMock.Verify(d => d.Action);
            while (!Vm.MainManipulateur.Finished)
                Thread.Sleep(5);
            Assert.IsTrue(sourceTime < File.GetLastWriteTime(destFilename));
            File.Delete(destFilename);
        }

        [Test]
        public void Go_whenSourceDoesntExists_ThenShowError()
        {
            //Arrange
            DlgMock.Setup(d => d.Inform("Erreur : le fichier Source n'existe pas. Choisi un autre fichier !", "La source n'existe pas"))
                .Returns(true);

            //Act
            bool result = Vm.Go(Dlg);

            //Assert
            Assert.IsFalse(result);
            DlgMock.Verify(d => d.Inform("Erreur : le fichier Source n'existe pas. Choisi un autre fichier !", "La source n'existe pas"));
        }

        [Test]
        public void Go_WhenDestinationDirectoryEmpty_ThenInform()
        {
            //Arrange
            Vm.MainManipulateur.DestinationDirectory = "";
            SetupValidSource(Ressources.UneSecondeFile);
            DlgMock.Setup(d => d.Inform("Tu n'as pas choisi la destination ! Choisi un répertoire de destination et recommance.", "Erreur"))
                .Returns(true);

            //Act
            bool result = Vm.Go(Dlg);

            //Assert
            Assert.IsFalse(result);
            DlgMock.Verify(d => d.Inform("Tu n'as pas choisi la destination ! Choisi un répertoire de destination et recommance.", "Erreur"));
        }
        #endregion

        #region User Settings Test
        //[Test]
        //public void UserSettings_WhenDestinationDirectoryChosen_ThenUpdateSetting()
        //{
        //    //Arrange
        //    Mock<IDialogService> dlgMock = new Mock<IDialogService>();
        //    IDialogService dlg = dlgMock.Object;
        //    string configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;
        //    DateTime configChange = File.GetLastWriteTime(configFile);

        //    //Act
        //    SetupValidDestination(dlgMock, dlg, Ressources.DestDir);

        //    //Assert
        //    Assert.AreEqual(Ressources.DestDir, Properties.Settings.Default.DestinationDirectory);
        //    Assert.IsTrue(configChange < File.GetLastWriteTime(configFile));
        //}

        //[Test]
        //public void UserSettings_WhenAppStarts_ThenLoadSettings()
        //{
        //    //Arrange
        //    Mock<IDialogService> dlgMock = new Mock<IDialogService>();
        //    IDialogService dlg = dlgMock.Object;
        //    SetupValidDestination(dlgMock, dlg, Ressources.DestDir);

        //    //Act
        //    var expectedVm = new MainViewModel();

        //    //Assert
        //    Assert.AreEqual(Ressources.DestDir, expectedVm.MainManipulateur.DestinationDirectory);
        //}
        #endregion
    }
}

