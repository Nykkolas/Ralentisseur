**Use Case:** 2 Convertir tous les fichiers

**Goal in Context:** Nicole converti tous les fichiers qui ne l'ont pas encore été précédemment d'un simple click

**Scope:** Manipulateur

**Level:** Primary task

**Primary Actor:** Nicole, et n'importe qui qui voudrait faire de même

**Priority:** Ca serait bien de réussir

**Frequency:** A chaque fois qu'elle a digitalisé plusieurs films

**Trigger** : Nicole a digitalisé plusieurs films et veut les convertir

**MAIN SUCCESS SCENARIO**  

1. Elle vérifie que tous les fichiers qu'elle vient de créer (ou qu'elle a créé dans la semaine) sont là
2. Elle "dit" GO pour tous les convertir
3. La conversion se fait avec un minimum de d'infos
4. Elle est prévenue quand tous les fichiers ont été convertis

