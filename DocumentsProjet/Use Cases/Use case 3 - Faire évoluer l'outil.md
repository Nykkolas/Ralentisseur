**Use Case:** 3 Faire évoluer l'outil

**Goal in Context:** Je dois pouvoir ajouter, retirer ou modifier des bouts de logiciel et le "déployer" simplement pour que Nicole l'utilise après, sans autre manipulation

**Scope:** Manipulateur

**Level:** Primary task

**Primary Actor:** Moi

**Priority:** Super important quand même, mais il est quand même possible de s'en passer

**Frequency:** Beaucoup au début et moins souvent après

**Trigger** : J'ai eut une idée ou Nicole m'a fait un commentaire / m'a signalé un bug

**MAIN SUCCESS SCENARIO**

1. En discutant avec Nicole, je m'aperçois qu'il faut changer quelque chose
2. Je mets à jour le Use case concerné
3. Je crée les User stories qui vont bien
4. Je fais les modifs dans le code
5. Je mets à jour la version utilisée par Nicole directement
6. Nicole peut utiliser la nouvelle version directement (sans installation ou autre manipulation)

**EXTENSIONS**  
Aucune

**SUB-VARIATIONS**  
> 1: Je peux m'apercevoir moi même qu'il y a quelque chose à changer  

4: Si possible, je montre les modifications faites avant de déployer pour m'assurer que ça convient.  
5: Déploiement automatique en option  
5: Option : afficher la date de la dernière mise à jour quelque part (dans un coin peu visible)

**Superordinate Use Case:** Aucun

**Subordinate Use Cases:** Aucun

**Performance Target:** ??

**OPEN ISSUES**  
Quelle "performance" conviendra ? TTM ?  

**SCHEDULE**  
A voir
