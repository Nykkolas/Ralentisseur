**Use Case:** 1 Convertir un fichier

**Goal in Context:** Nicole converti un fichier film, numérisé avec sa machine, pour qu'il passe à la bonne vitesse (par défaut le super 8 enregistre en 18 fps mais les fichiers semblent sortir en 30 fps)

**Scope:** Manipulateur

**Level:** Primary task

**Primary Actor:** Nicole, et n'importe qui qui voudrait faire comme elle

**Priority:** Indispensable

**Frequency:** Pour chaque film numérisé

**Trigger** : Nicole a fini de numériser un, ou plusieurs films

**MAIN SUCCESS SCENARIO**  

1. Nicole choisi un fichier à convertir (sélecteur de fichiers classique)
2. Elle choisi l'endroit où le film converti doit aller (la destination, un répertoire)
3. Elle "dit" GO
4. Le film se converti, en montrant "Conversion en cours..."
5. Une fois le film converti, Nicole est prévenue

**EXTENSIONS**  
2a: La destination précédemment choisie n'existe plus.  
2a1: Choisir le "home" de l'utilisateur par défaut  
> 3a: La source et la destination sont les mêmes  
3a1: Prévenir que le fichier sera nommé "nom de la source (nouveau).mp4"  
3a2: Donner le choix : Continuer comme ça (3a21) OU Annuler (3a22) et choisir une autre destination  
3a21: Continuer comme ça = faire la conversion en prenant en compte le nouveau nom de fichier  
3a22: Annuler et choisir une autre destination = retour à l'écran principal
> 3b: Le fichier destination existe déjà  
> 3b1: Prévenir que le fichier destination existe déjà et donner clairement son nom  
> 3b2: donner le choix entre : Annuler (3b21), Conserver (3b22) ou Remplacer (3b23)  
> 3b21: retourner en 1  
> 3b22: Utiliser "\<nom du fichier destination> (nouveau).mp4" comme nom de destination  
> 3b23: Ecraser le fichier existant  

3c: Nicole n'a pas les droits pour écrire dans le répertoire destination

> 3d: Cliquer sur GO avant de choisir la source ou la destination  
> 3d1: Afficher un message d'erreur disant de choisir quelque chose*


**SUB-VARIATIONS**  
1a: Configurer le répertoire "partage famille" par défaut  
1b: Permettre de glisser un fichier sur une zone particulière de l'application.  
1c: Si je crée un sélecteur classique, se rappeler du dernier répertoire choisi.  
1d: Afficher quelques informations utiles sur le fichier (facteur de ralentissement par exemple).  
2a: Se rappeler de la destination choisie les fois d'avant.  
2b: Permettre de faire glisser la destination  
4a: Montrer une barre de progression (100% = conversion terminée).  
4b: Montrer un timer (temps écoulé)  
4c: Afficher le temps de conversion et la duree du film dans la boite de dialogue « terminé »  

**Superordinate Use Case:** Aucun

**Subordinate Use Cases:** Aucun

**Performance Target:** Les étapes 1 et 2 doivent être "faciles" pour Nicole

**OPEN ISSUES**  

**SCHEDULE**  
