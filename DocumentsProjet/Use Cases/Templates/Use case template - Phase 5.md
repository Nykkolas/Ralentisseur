**Channel to primary actor:** \<e.g. interactive, static files, database>

**Secondary Actors:** \<list of other systems needed to accomplish use case>

**Channel to Secondary Actors:** \<e.g. interactive, static, file, database, timeout>
