
**Source :** [Basic use case template][Source]
[Source]: http://alistair.cockburn.us/Basic+use+case+template  

**Use Case:** \<number> \<the name should be the goal as a short active verb phrase>

**Goal in Context:** \<a longer statement of the goal, if needed>

**Scope:** \<what system is being considered black-box under design>

**Level:** \<one of: Summary, Primary task, Subfunction>

**Primary Actor:** \<a role name for the primary actor, or description>

**Priority:** \<how critical to your system / organization>

**Frequency:** \<how often it is expected to happen>
