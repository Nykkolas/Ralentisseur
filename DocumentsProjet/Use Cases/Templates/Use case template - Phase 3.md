**EXTENSIONS**  
\<step altered> \<condition> : \<action or sub.use case>  
\<step altered> \<condition> : \<action or sub.use case>

**SUB-VARIATIONS**  
\<step or variation # > \<list of sub-variations>  
\<step or variation # > \<list of sub-variations>

**Superordinate Use Case:** \<optional, name of use case that includes this one>

**Subordinate Use Cases:** \<optional, depending on tools, links to sub.use cases>
