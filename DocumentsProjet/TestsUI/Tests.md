## Préparation
Supprimer les fichiers du répertoire destination

## Scénario 1
### Arrange
1. Choisir le fichier source
2. Choisir le répertoire destination vide

### Act
Cliquer sur GO

### Assert
1. Le nom de fichier est renseigné dans le label "destination"  
2. Un fichier est créé dans le répertoire de destination

## Scénario 2
### Arrange
1. Sélectionner un autre fichier source (nom différent)

### Act
Cliquer sur GO

### Assert
1. Le nom de fichier est renseigné dans le label "destination"  
2. Un autre fichier est créé dans le répertoire destination

## Scénario 3
### Arrange
1. Fermer l'application
2. Relancer l'application
3. Choisir le même fichier que dans le scénario 1
4. Choisir la même destination que le scénario 1

### Act
Cliquer sur GO

### Assert
Une boite de dialogue apparaît proposant d'annuler, conserver ou remplacer le fichier destination

## Scénario 3.1
### Arrange
Scénario 3

### Act
Cliquer sur "Annuler"

### Assert
Retour à la fenêtre principale

## Scénario 3.2
### Arrange
Scénario 3

### Act
Cliquer sur "Conserver"

### Assert
1. Le label destination est mis à jour avec "nom de fichier (nouveau).mp4"
2. Le fichier "nom de fichier (nouveau).mp4" est créé

## Scénario 3.3
### Arrange
Scénario 3

### Act
Cliquer sur "Remplacer"

### Assert
Le fichier destination est remplacé (vérifier la date de création) (sous Windows la date de création n'est pas changée...)

## Scénario 4
### Arrange
1. Fermer l'application
2. Lancer l'application

### Act
Cliquer sur "Go"

### Assert
Un message d'erreur apparait
