﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using ManipulateurFFMpeg.Library;

namespace Program
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.Write("Faire glisser le fichier à ralentir : ");
            string inputFile = Console.ReadLine().Trim();

            Console.Write("Faire glisser le répertoire de destination : ");
            string outputDir = Console.ReadLine().Trim();

            Manipulateur mainManipulateur = new Manipulateur(inputFile, outputDir);
            //Manipulateur mainManipulateur = new Manipulateur(inputFile, Path.Combine(outputDir, "output.mp4"));
            //"/Users/nicolasfournier/CloudStation/Divers/TestsConvertisseur/Fichiertest.mp4",
            //"/Users/nicolasfournier/CloudStation/Divers/TestsConvertisseur/output.mp4");

            Console.WriteLine("Voici le fichier à transformer : " + mainManipulateur.SourceFileName);
            Console.WriteLine("Voici la destination du fichier transformé : " + mainManipulateur.DestinationFileName);
            //Tester la présence, si il est là, stop...


            Console.WriteLine("Voici son framerate : " + mainManipulateur.getFrameRate() + " fps alors qu'il devrait être à 18 fps !!!");

            Console.WriteLine("Il suffit donc de le ralentir de " + mainManipulateur.getSlowdownRatio() + "(soit 18/" + mainManipulateur.getFrameRate() + ")");

            Task mainManipulateurTask = mainManipulateur.StartConversion();

            //Ne fonctionne pas dans Main apparemment...
            //System.Timers.Timer mTimer = new System.Timers.Timer();
            //mTimer.Interval = 1000;
            ////mTimer.AutoReset = true;
            //mTimer.Elapsed += (s, e) =>
            //{
            //    if (mainManipulateurTask.IsCompleted)
            //    {
            //        Console.WriteLine("\nConversion terminée");
            //        mTimer.Stop();
            //    }
            //    else
            //    {
            //        Console.WriteLine("Conversion en cours...");
            //    }
            //};
            //mTimer.Start();

            while (!mainManipulateurTask.IsCompleted)
            {
                Console.WriteLine("Conversion en cours...");
                Thread.Sleep(1000);
            }
        }
    }
}
