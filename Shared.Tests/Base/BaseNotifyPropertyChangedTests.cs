﻿using NUnit.Framework;
using Shared.Base;
using System;
using System.ComponentModel;

namespace Shared.Tests.Base
{
    class A : BaseNotifyPropertyChanged
    {
        private string _p;
        public string P
        {
            get => _p;
            set => SetProperty(ref _p, value);
        }
    }

    [TestFixture()]
    public class BaseNotifyPropertyChangedTests
    {
        [Test()]
        public void SetProperty_WhenSet_ThenRaiseEvent()
        {
            //Arrange
            var a = new A();
            string expected = "toto";
            string result = "";
            a.PropertyChanged += (object sender, PropertyChangedEventArgs e) => { result = e.PropertyName; };

            //Act
            a.P = expected;

            //Assert
            Assert.AreEqual("P", result);
        }
    }
}
