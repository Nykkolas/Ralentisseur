﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Threading;
using NUnit.Framework;
using Xabe.FFmpeg;

namespace ManipulateurFFMpeg.Library.Tests
{
    [TestFixture]
    public class ManipulateurTests
    {
        #region Construtors
        [Test]
        public static void NewManipulateur_WhenSourceFileThenDestDir_SetConcatenateDestFile()
        {
            string sourceFile = Ressources.UneSecondeFile;
            string destDir = Path.Combine(Path.GetDirectoryName(Ressources.UneSecondeFile), "Output");

            Manipulateur resultManipulateur = new Manipulateur(sourceFile);
            resultManipulateur.DestinationDirectory = destDir;

            Assert.AreEqual(sourceFile, resultManipulateur.SourceFileName);
            Assert.AreEqual(destDir, resultManipulateur.DestinationDirectory);
            Assert.AreEqual(Path.Combine(destDir, Path.GetFileName(sourceFile)), resultManipulateur.DestinationFileName);
        }

        [Test]
        public static void NewManipulateur_WhenSourceFileAndDestDir_SetConcatenateDestFile()
        {
            string sourceFile = Ressources.UneSecondeFile;
            string destDir = Path.Combine(Path.GetDirectoryName(Ressources.UneSecondeFile), "Output");

            Manipulateur resultManipulateur = new Manipulateur(sourceFile, destDir);

            Assert.AreEqual(sourceFile, resultManipulateur.SourceFileName);
            Assert.AreEqual(destDir, resultManipulateur.DestinationDirectory);
            Assert.AreEqual(Path.Combine(destDir, Path.GetFileName(sourceFile)), resultManipulateur.DestinationFileName);
        }
        [Test]
        public static void NewManipulateur_WhenDestDirThenSourceDir_SetConcatenateDestFile()
        {
            string sourceFile = Ressources.UneSecondeFile;
            string destDir = Path.Combine(Path.GetDirectoryName(Ressources.UneSecondeFile), "Output");

            Manipulateur resultManipulateur = new Manipulateur();
            resultManipulateur.DestinationDirectory = destDir;
            resultManipulateur.SourceFileName = sourceFile;

            Assert.AreEqual(sourceFile, resultManipulateur.SourceFileName);
            Assert.AreEqual(destDir, resultManipulateur.DestinationDirectory);
            Assert.AreEqual(Path.Combine(destDir, Path.GetFileName(sourceFile)), resultManipulateur.DestinationFileName);
        }
        #endregion

        #region SetUp
        private static List<string> SetupEventsNamesList(Manipulateur inputManipulateur)
        {
            List<string> actual = new List<string>();
            inputManipulateur.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e)
            {
                actual.Add(e.PropertyName);
            };
            return actual;
        }
        #endregion

        #region Private Methods
        private static bool ExistsEventName(List<string> actual, string search)
        {
            return actual.Exists(s => s == search);
        }
        #endregion

        [Test]
        public static void getFileLocation_WhenConstructorEmpty_ReturnsFileLocationEmpty()
        {
            Manipulateur inputManipulateur = new Manipulateur();

            Assert.AreEqual("", inputManipulateur.SourceFileName);
        }

        [Test]
        public static void getFileLocation_WhenConstructorWithFilename_ReturnsFileLocation()
        {
            string movieFileName = Ressources.MovieTestFile;
            Manipulateur inputManipulateur = new Manipulateur(movieFileName);

            Assert.AreEqual(movieFileName, inputManipulateur.SourceFileName);
        }


        [Test]
        public static void getFrameRate_WhenConstructorEmpty_ReturnsMinusOne()
        {
            Manipulateur inputManipulateur = new Manipulateur();

            Assert.AreEqual(-1, inputManipulateur.getFrameRate());
        }

        [Test]
        public static void getFrameRate_WhenConstructorWithFileName_ReturnsFileFrameRate()
        {
            string movieFileName = Ressources.MovieTestFile;
            Manipulateur inputManipulateur = new Manipulateur(movieFileName);

            Assert.AreEqual(29.969999999999999, inputManipulateur.getFrameRate());
        }

        [Test]
        public static void calculateSlowdownRatio_WhenZero_ReturnsMinusOne()
        {
            Manipulateur inputManipulateur = new Manipulateur();

            double result = inputManipulateur.getSlowdownRatio();

            Assert.AreEqual(-1, result);
        }

        [Test]
        public static void calculateSlowdownRatio_WhenFilename_ReturnsSlowdownRatioTo18()
        {
            string movieFileName = Ressources.MovieTestFile;
            Manipulateur inputManipulateur = new Manipulateur(movieFileName);

            double result = inputManipulateur.getSlowdownRatio();

            Assert.AreEqual(0.60060060060060061, result);
        }

        [Test]
        public static void getOutputLocation_WhenEmptyConstructor_ReturnsEmptyString()
        {
            Manipulateur inputManipulateur = new Manipulateur();

            Assert.AreEqual("", inputManipulateur.DestinationFileName);
        }

        [Test]
        public static void getOutputLocation_WhenConstructorWithFilename_ReturnsOutputFilename()
        {
            string movieFileName = Ressources.MovieTestFile;
            string destinationFileName = Ressources.DestinationFileName;
            Manipulateur inputManipulateur = new Manipulateur(movieFileName, destinationFileName);

            Assert.AreEqual(destinationFileName, inputManipulateur.DestinationFileName);
        }

        [Test]
        public void SetSourceFileName_WhenUsedAfterDefaultConstructor_UpdateSourceVideoInfo()
        {
            Manipulateur inputManipulateur = new Manipulateur();

            inputManipulateur.SourceFileName = Ressources.UneSecondeFile;

            Assert.AreEqual(30, inputManipulateur.getFrameRate());
        }

        [Test]
        public void SetSourceFileName_WhenChangeSourceFileName_UpdateSourceVideoInfo()
        {
            Manipulateur inputManipulateur = new Manipulateur(Ressources.UneSecondeFile);

            inputManipulateur.SourceFileName = Ressources.MovieTestFile;

            Assert.AreEqual(Ressources.MovieTestFile, inputManipulateur.SourceVideoInfo.FileInfo.FullName);
        }

        #region CheckConversionReady
        [Test]
        public void CheckConversionReady_WhenReady_ReturnsTrueAndErrorStatusOk()
        {
            Manipulateur inputManipulateur = new Manipulateur(Ressources.UneSecondeFile, Ressources.DestinationFileName);

            if (File.Exists(Ressources.DestinationFileName))
                File.Delete(Ressources.DestinationFileName);

            bool result = inputManipulateur.CheckConversionReady();

            Assert.True(result);
            Assert.AreEqual(ConversionStatus.Ok, inputManipulateur.ErrorStatus);
        }

        [Test]
        public void CheckConversionReady_WhenDestinationFileNameExists_ReturnsFalseAndErrorStatus()
        {
            Manipulateur inputManipulateur = new Manipulateur(Ressources.UneSecondeFile, Ressources.DummyOutput);

            bool result = inputManipulateur.CheckConversionReady();

            Assert.False(result);
            Assert.AreEqual(ConversionStatus.DestFileAlreadyExists, inputManipulateur.ErrorStatus);
        }

        [Test]
        public void CheckConversionReady_WhenInputFileDoesntExists_ReturnsFalseAndErrorStatus()
        {
            Manipulateur inputManipulateur = new Manipulateur();

            bool result = inputManipulateur.CheckConversionReady();

            Assert.False(result);
            Assert.AreEqual(ConversionStatus.SourceDoesntExists, inputManipulateur.ErrorStatus);
        }

        [Test]
        public void CheckConversionReady_WhenSourceAndDestinationAreSame_ReturnsFalseAndErrorStatus()
        {
            Manipulateur inputManipulateur = new Manipulateur(Ressources.UneSecondeFile, Path.GetDirectoryName(Ressources.UneSecondeFile));

            bool result = inputManipulateur.CheckConversionReady();

            Assert.False(result);
            Assert.AreEqual(ConversionStatus.SourceAndDestSame, inputManipulateur.ErrorStatus);
        }

        [Test]
        public void CheckConversionReady_WhenDestinationDirectoryEmpty_ThenReturnsFalseAndErrorStatus()
        {
            var inputManipulateur = new Manipulateur(Ressources.MovieTestFile);

            bool result = inputManipulateur.CheckConversionReady();

            Assert.False(result);
            Assert.AreEqual(ConversionStatus.NoDestDirChosen, inputManipulateur.ErrorStatus);
        }
        #endregion

        #region StartConversion
        [Test]
        public static void startConversion_WhenUneSeconde_ReturnsOutput()
        {
            if (File.Exists(Ressources.DestinationFileName))
                File.Delete(Ressources.DestinationFileName);

            Manipulateur inputManipulateur = new Manipulateur(Ressources.UneSecondeFile, Ressources.DestinationFileName);

            inputManipulateur.StartConversion();
            while (!inputManipulateur.Finished)
                Thread.Sleep(5);

            IMediaInfo destinationInfo = new MediaInfo(Ressources.DestinationFileName);

            Assert.True(inputManipulateur.Finished);
            Assert.AreEqual(30, destinationInfo.Properties.FrameRate);
        }

        //[Test]
        //public static void StartConversion_WhenDestinationIsDirectory_UpdateDestinationFileName()
        //{
        //    string outputFile = Path.Combine(
        //        Path.GetDirectoryName(Ressources.DestinationFileName),
        //        "Output",
        //        Path.GetFileName(Ressources.UneSecondeFile));
        //    if (File.Exists(outputFile))
        //        File.Delete(outputFile);

        //    Manipulateur inputManipulateur = new Manipulateur(Ressources.UneSecondeFile);
        //    inputManipulateur.DestinationFileName = Path.GetDirectoryName(outputFile);

        //    inputManipulateur.StartConversion();
        //    while (!inputManipulateur.Finished)
        //        Thread.Sleep(1);

        //    IMediaInfo destinationInfo = new MediaInfo(outputFile);

        //    Assert.True(inputManipulateur.Finished);
        //    Assert.AreEqual(30, destinationInfo.Properties.FrameRate);
        //}

        [Test]
        public void StartConversion_WhenDestinationFileExists_StopAndSendError()
        {
            Manipulateur inputManipulateur = new Manipulateur(Ressources.UneSecondeFile, Ressources.DummyOutput);

            inputManipulateur.StartConversion();
            while (!inputManipulateur.Finished)
                Thread.Sleep(5);

            Assert.AreEqual(ConversionStatus.DestFileAlreadyExists, inputManipulateur.ErrorStatus);
        }

        [Test]
        public void StartConversion_WhenSourceAndDestinationTheSame_StopAndSendError()
        {
            Manipulateur inputManipulateur = new Manipulateur(Ressources.UneSecondeFile,
                                                              Path.GetDirectoryName(Ressources.UneSecondeFile));

            inputManipulateur.StartConversion();
            while (!inputManipulateur.Finished)
                Thread.Sleep(1);

            Assert.AreEqual(ConversionStatus.SourceAndDestSame, inputManipulateur.ErrorStatus);
        }

        [Test]
        public void StartConversion_WhenRunTwoTimesInARow_Works()
        {
            Manipulateur inputManipulateur = new Manipulateur(Ressources.UneSecondeFile, Ressources.DestinationFileName);

            if (File.Exists(Ressources.DestinationFileName))
                File.Delete(Ressources.DestinationFileName);

            inputManipulateur.StartConversion();
            while (!inputManipulateur.Finished)
                Thread.Sleep(5);

            if (File.Exists(Ressources.DestinationFileName))
                File.Delete(Ressources.DestinationFileName);

            inputManipulateur.StartConversion();
            Assert.False(inputManipulateur.Finished);
            while (!inputManipulateur.Finished)
                Thread.Sleep(5);

            Assert.True(inputManipulateur.Finished);
        }
        #endregion

        #region StartConversionWithSameFileName
        [Test]
        public void StartConversionDestinationExists_WhenRemplacer_ReplaceAndConvert()
        {

            Manipulateur inputManipulateur = new Manipulateur(Ressources.UneSecondeFile, Ressources.DummyOutput);
            DestExistsActions action = DestExistsActions.Remplacer;
            DateTime now = new DateTime();
            now = DateTime.Now;

            bool result = inputManipulateur.StartConversionDestinationExists(action);
            while (!inputManipulateur.Finished)
                Thread.Sleep(5);

            Assert.True(result);
            Assert.LessOrEqual(now, File.GetLastWriteTime(Ressources.DummyOutput));

        }
        [Test]
        public void StartConversionDestinationExists_WhenConserver_ChangeDestinationAndConvert()
        {
            Manipulateur inputManipulateur = new Manipulateur(Ressources.UneSecondeFile, Ressources.DummyOutput);
            DestExistsActions action = DestExistsActions.Conserver;
            string destinationFilnameWithoutExtension = Path.GetFileNameWithoutExtension(Ressources.DummyOutput);
            string destinationExtension = Path.GetExtension(Ressources.DummyOutput);
            string destinationDirectory = Path.GetDirectoryName(Ressources.DummyOutput);
            string expectedFilename = Path.Combine(
                destinationDirectory,
                destinationFilnameWithoutExtension + " (nouveau)" + destinationExtension);

            if (File.Exists(expectedFilename))
                File.Delete(expectedFilename);

            bool result = inputManipulateur.StartConversionDestinationExists(action);
            while (!inputManipulateur.Finished)
                Thread.Sleep(5);

            Assert.IsTrue(result);
            Assert.IsTrue(File.Exists(expectedFilename));
            Assert.IsTrue(File.Exists(Ressources.DummyOutput));
        }
        #endregion

        #region PropertySourceFileNameChanged Tests
        [Test]
        public void PropertySourceFileNameChanged_WhenChanged_ThenRaiseEvent()
        {
            //Arrange
            var inputManipulateur = new Manipulateur();
            List<string> actual = SetupEventsNamesList(inputManipulateur);

            //Act
            inputManipulateur.SourceFileName = Ressources.UneSecondeFile;

            //Assert
            Assert.AreEqual(Ressources.UneSecondeFile, inputManipulateur.SourceFileName);
            Assert.IsTrue(ExistsEventName(actual, nameof(inputManipulateur.SourceFileName)));
        }
        #endregion

        #region PropertyDestinationFileNameChanged Tests
        [Test]
        public void PropertyDestinationFileNameChanged_WhenChanged_ThenRaiseEvent()
        {
            //Arrange
            var inputManipulateur = new Manipulateur(Ressources.UneSecondeFile);
            List<string> actual = SetupEventsNamesList(inputManipulateur);

            //Act
            inputManipulateur.DestinationDirectory = Ressources.OutputDir;

            //Assert
            Assert.IsTrue(ExistsEventName(actual, "DestinationFileName"));
            Assert.AreEqual(Path.Combine(Ressources.OutputDir, Path.GetFileName(Ressources.UneSecondeFile)), inputManipulateur.DestinationFileName);
        }

        [Test]
        public void PropertyDestinationFileNameChanged_WhenSourceFileNameChanged_ThenRaiseEvent()
        {
            //Arrange
            var inputManipulateur = new Manipulateur(Ressources.UneSecondeFile, Ressources.OutputDir);
            List<string> actual = SetupEventsNamesList(inputManipulateur);
            
            //Act
            inputManipulateur.SourceFileName = Ressources.MovieTestFile;

            //Assert
            Assert.AreEqual(Path.Combine(Ressources.OutputDir, Path.GetFileName(Ressources.MovieTestFile)), inputManipulateur.DestinationFileName);
            Assert.IsTrue(ExistsEventName(actual, "DestinationFileName"));
        }
        #endregion

        #region PropertyDestinationFileNameChanged Tests
        [Test]
        public void PropertyDestinationDirChanged_WhenChanged_ThenRaiseEvent()
        {
            //Arrange
            var inputManipulateur = new Manipulateur();
            List<string> actual = SetupEventsNamesList(inputManipulateur);

            //Act
            inputManipulateur.DestinationDirectory = Ressources.OutputDir;

            //Assert
            Assert.AreEqual(Ressources.OutputDir, inputManipulateur.DestinationDirectory);
            Assert.IsEmpty(inputManipulateur.SourceFileName);
            Assert.IsTrue(ExistsEventName(actual, "DestinationDirectory"));
        }
        #endregion
    }
}
