﻿using System;
using System.IO;
using System.Reflection;

namespace ManipulateurFFMpeg.Library.Tests
{
    internal static class Ressources
    {
        internal static readonly string MovieTestFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Ressources", "TestFiles", "Fichier test.mp4");
        internal static readonly string DestinationFileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Ressources", "TestFiles", "output.mp4");
        internal static readonly string UneSecondeFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Ressources", "TestFiles", "UneSeconde.mp4");
        internal static readonly string DummyOutput = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Ressources", "TestFiles", "DummyOutput.mp4");
        internal static readonly string OutputDir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Ressources", "TestFiles", "Output");
    }
}
