﻿using System;
using ManipulateurFFMpeg.Library;
using Ralentisseur.Common.Services;

namespace Ralentisseur.Common.ViewModels
{
    public class CommonViewModel
    {
        #region Private Properties
        protected Manipulateur _mainManipulateur;
        #endregion

        #region Public Properties
        public Manipulateur MainManipulateur
        {
            get { return _mainManipulateur; }
        }
        #endregion

        #region Ctor
        public CommonViewModel()
        {
            _mainManipulateur = new Manipulateur();
        }
        #endregion

        #region Public Methods
        public void ChooseSource(IDialogService dlg)
        {
            if (dlg.AskSource())
            {
                if (_mainManipulateur == null)
                    _mainManipulateur = new Manipulateur(dlg.Result);
                else
                    _mainManipulateur.SourceFileName = dlg.Result;
            } else {
                Console.WriteLine("Cancel");
            }
        }

        public void ChooseDestination(IDialogService dlg)
        {
            if (dlg.AskDestination(_mainManipulateur.DestinationDirectory))
            {
                if (_mainManipulateur == null)
                {
                    _mainManipulateur = new Manipulateur();
                    _mainManipulateur.DestinationDirectory = dlg.Result;
                }
                else
                {
                    _mainManipulateur.DestinationDirectory = dlg.Result;
                }
            }
        }

        public bool Go(IDialogService dlg)
        {
            if (!_mainManipulateur.CheckConversionReady())
            {
                switch (_mainManipulateur.ErrorStatus)
                {
                    case ConversionStatus.DestFileAlreadyExists:
                        if (dlg.AskDestinationAlreadyExists(_mainManipulateur.DestinationFileName, _mainManipulateur.GetSuffixDestinationFileName()))
                        {
                            _mainManipulateur.StartConversionDestinationExists(dlg.Action);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case ConversionStatus.SourceAndDestSame:
                        if (dlg.AskSourceAndDestSame(_mainManipulateur.GetSuffixDestinationFileName()))
                        {
                            _mainManipulateur.StartConversionDestinationExists(dlg.Action);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case ConversionStatus.SourceDoesntExists:
                        dlg.Inform("Erreur : le fichier Source n'existe pas. Choisi un autre fichier !", "La source n'existe pas");
                        return false;
                    case ConversionStatus.NoDestDirChosen:
                        dlg.Inform("Tu n'as pas choisi la destination ! Choisi un répertoire de destination et recommance.", "Erreur");
                        return false;
                    default:
                        dlg.Inform("Merci de choisir un fichier source et un répertoire destination", "");
                        return false;
                }
            }

            _mainManipulateur.StartConversion();
            return true;
        }
        #endregion
    }
}
