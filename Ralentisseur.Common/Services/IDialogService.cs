﻿using ManipulateurFFMpeg.Library;

namespace Ralentisseur.Common.Services
{
    public interface IDialogService
    {
        string Result { get; }
        DestExistsActions Action { get; }
        bool AskSource();
        bool AskDestination(string currentDestDir = "");
        bool Inform(string information, string titre);
        bool AskDestinationAlreadyExists(string filename, string keepFilename);
        bool AskSourceAndDestSame(string filename);
    }
}
