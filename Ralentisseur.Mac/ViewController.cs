﻿using System;
using System.ComponentModel;
using AppKit;
using Foundation;
using Ralentisseur.Common.Services;
using Ralentisseur.Mac.Services;
using Ralentisseur.Mac.ViewModels;

namespace Ralentisseur.Mac
{
    public partial class ViewController : NSViewController
    {
        #region Private Properties
        private MainViewModel _mainViewModel;
        private IDialogService _dlgService;
        #endregion

        #region Constructors
        public ViewController(IntPtr handle) : base(handle)
        {
        }
        #endregion

        #region Override Methods
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            _mainViewModel = new MainViewModel();
            _dlgService = new DialogServices();

            //string destUrl = DestinationPath.Url.ToString();
            //_mainViewModel.MainManipulateur.DestinationDirectory = destUrl.Substring(destUrl.IndexOf(':') + 1);

            _mainViewModel.MainManipulateur.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e)
            {
                if (e.PropertyName == nameof(_mainViewModel.MainManipulateur.SourceFileName))
                {
                    RefreshSourcePath();
                }
                if (e.PropertyName == nameof(_mainViewModel.MainManipulateur.DestinationFileName)
                    || e.PropertyName == nameof(_mainViewModel.MainManipulateur.DestinationDirectory))
                {
                    RefreshDestinationPath();
                }
            };
        }

        public override NSObject RepresentedObject
        {
            get
            {
                return base.RepresentedObject;
            }
            set
            {
                base.RepresentedObject = value;
                // Update the view, if already loaded.
            }
        }
        #endregion

        #region Private Methods
        private void ShowAlert(string message)
        {
            NSAlert alert = new NSAlert();

            alert.MessageText = message;
            alert.RunModal();
        }

        private void WaitConversionToFinish(OngoingViewController ongoingSheet)
        {
            NSTimer.CreateRepeatingScheduledTimer(1, (obj) =>
            {
                if (_mainViewModel.MainManipulateur.Finished)
                {
                    ShowAlert("Conversion terminée !");
                    this.DismissViewController(ongoingSheet);
                    obj.Invalidate();
                }
                else
                {
                    Console.WriteLine("Conversion en cours...");
                }
            });
        }

        private void RefreshSourcePath()
        {
            if (!String.IsNullOrEmpty(_mainViewModel.MainManipulateur.SourceFileName))
                SourcePath.Url = new Uri(_mainViewModel.MainManipulateur.SourceFileName);   
        }

        private void RefreshDestinationPath()
        {
            if (!String.IsNullOrEmpty(_mainViewModel.MainManipulateur.DestinationFileName))
                DestinationPath.Url = new Uri(_mainViewModel.MainManipulateur.DestinationFileName);
            else if (!String.IsNullOrEmpty(_mainViewModel.MainManipulateur.DestinationDirectory))
                DestinationPath.Url = new Uri(_mainViewModel.MainManipulateur.DestinationDirectory);
        }
        #endregion

        #region Action Methods
        partial void ChooseSource(NSObject sender)
        {
            _mainViewModel.ChooseSource(_dlgService);
        }

        partial void ChooseDestination(NSObject sender)
        {
            _mainViewModel.ChooseDestination(_dlgService);
        }
		#endregion

		#region Override Methods
		public override bool ShouldPerformSegue(string identifier, NSObject sender)
		{
            switch (identifier) 
            {
                case "GoSegue":
                    return _mainViewModel.Go(_dlgService);
            };
            return base.ShouldPerformSegue(identifier, sender);
        }

		public override void PrepareForSegue(NSStoryboardSegue segue, NSObject sender)
		{
    		base.PrepareForSegue(segue, sender);

    		switch (segue.Identifier)
    		{
    		    case "GoSegue":
    		        var ongoingSheet = segue.DestinationController as OngoingViewController;
                    WaitConversionToFinish(ongoingSheet);

		            break;
    		}
    	}
		#endregion
	}
}
