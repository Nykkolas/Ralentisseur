﻿using System;
using System.IO;
using AppKit;
using ManipulateurFFMpeg.Library;
using Ralentisseur.Common.Services;

namespace Ralentisseur.Mac.Services
{
    public class DialogServices : IDialogService
    {
        #region Private Properties
        private string _result;
        private DestExistsActions _action;
        #endregion

        #region Public Properties
        public string Result
        {
            get => _result;
        }

        public DestExistsActions Action
        {
            get => _action;
        }
        #endregion

        #region Ctor
        public DialogServices()
        {

        }
        #endregion

        #region Private Methods
        private nint ShowDestExistsChoiceAlert(string filename, string keepFilename)
        {
            NSAlert alert = new NSAlert();

            alert.MessageText = "Le fichier" + Path.GetFileName(filename) + " existe déjà !";
            alert.InformativeText = "Le fichier que tu as choisi comme destination existe déjà.\n\n" +
                "Tu peux :\n" +
                "- Cliquer sur Annuler pour annuler la conversion et revenir à la fenêtre précédente\n" +
                "- Cliquer sur Conserver pour créer un nouveau fichier qui se nommera \"" + Path.GetFileName(keepFilename) + "\"\n" +
                "- Cliquer sur Remplacer pour l'effacer et le remplacer\n";
            alert.AddButton("Remplacer");
            alert.AddButton("Conserver");
            alert.AddButton("Annuler");

            return alert.RunModal();
        }

        private nint ShowSourceSameDestChoiceAlert(string keepFilename)
        {
            NSAlert alert = new NSAlert();

            alert.MessageText = "Les fichiers source et destination sont les mêmes !";
            alert.InformativeText = "Le fichier que tu as choisi comme destination est le même que la destination.\n\n" +
                "Tu peux :\n" +
                "- Cliquer sur Annuler pour annuler la conversion et revenir à la fenêtre précédente\n" +
                "- Cliquer sur Continuer pour créer un nouveau fichier qui se nommera \"" + Path.GetFileName(keepFilename) + "\"\n\n" +
                "Quelquesoit ton choix, le fichier source sera conservé !";
            alert.AddButton("Continuer");
            alert.AddButton("Annuler");

            return alert.RunModal();
        }
        #endregion

        #region Interface Implementation
        public bool AskSource()
        {
            NSOpenPanel dlg = NSOpenPanel.OpenPanel;
            dlg.CanChooseFiles = true;
            dlg.CanChooseDirectories = false;
            dlg.AllowedFileTypes = new string[] { "mp4" };

            if (dlg.RunModal() == 1)
            {
                _result = dlg.Url.Path;
                return true;
            }
            else
            {
                _result = "";
                return false;
            }
        }

        public bool AskDestination(string currentDestDir = "")
        {
            NSOpenPanel dlg = NSOpenPanel.OpenPanel;
            dlg.CanChooseFiles = false;
            dlg.CanChooseDirectories = true;
            if (!String.IsNullOrEmpty(currentDestDir))
                dlg.Directory = currentDestDir;

            nint result = dlg.RunModal();

            if (result == 1)
            {
                _result = dlg.Url.Path;
                return true;
            }
            else
            {
                _result = "";
                return false;
            }
        }

        public bool Inform(string information, string title)
        {
            NSAlert alert = new NSAlert();

            alert.MessageText = information;
            alert.RunModal();
            return true;
        }

        public bool AskDestinationAlreadyExists(string filename, string keepFilename)
        {
            switch (ShowDestExistsChoiceAlert(filename, keepFilename))
            {
                case 1000:
                    _action = DestExistsActions.Remplacer;
                    return true;
                case 1001:
                    _action = DestExistsActions.Conserver;
                    return true;
                case 1002:
                    _action =  DestExistsActions.Annuler;
                    return false;
                default:
                    _action = DestExistsActions.Annuler;
                    return false;
            }
        }

        public bool AskSourceAndDestSame(string keepFilename)
        {
            switch (ShowSourceSameDestChoiceAlert(keepFilename))
            {
                case 1000:
                    _action = DestExistsActions.Conserver;
                    return true;
                case 1001:
                    _action = DestExistsActions.Annuler;
                    return false;
                default:
                    return false;
            }
        }
        #endregion
    }
}
