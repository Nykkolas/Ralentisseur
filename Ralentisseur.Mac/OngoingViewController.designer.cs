// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Ralentisseur.Mac
{
	[Register ("OngoingViewController")]
	partial class OngoingViewController
	{
		[Outlet]
		AppKit.NSTextField OngoingStatus { get; set; }

		[Action ("CloseOngoing:")]
		partial void CloseOngoing (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (OngoingStatus != null) {
				OngoingStatus.Dispose ();
				OngoingStatus = null;
			}
		}
	}
}
