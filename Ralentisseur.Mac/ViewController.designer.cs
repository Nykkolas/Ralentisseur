// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Ralentisseur.Mac
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		AppKit.NSPathControl DestinationPath { get; set; }

		[Outlet]
		AppKit.NSButton GOButton { get; set; }

		[Outlet]
		AppKit.NSTextField OngoingLabel { get; set; }

		[Outlet]
		AppKit.NSPathControl SourcePath { get; set; }

		[Action ("ChooseDestination:")]
		partial void ChooseDestination (Foundation.NSObject sender);

		[Action ("ChooseSource:")]
		partial void ChooseSource (Foundation.NSObject sender);

		[Action ("StartConversion:")]
		partial void StartConversion (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (DestinationPath != null) {
				DestinationPath.Dispose ();
				DestinationPath = null;
			}

			if (SourcePath != null) {
				SourcePath.Dispose ();
				SourcePath = null;
			}

			if (OngoingLabel != null) {
				OngoingLabel.Dispose ();
				OngoingLabel = null;
			}

			if (GOButton != null) {
				GOButton.Dispose ();
				GOButton = null;
			}
		}
	}
}
