﻿using System;
using System.IO;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ralentisseur.Windows.ViewModels;
//using Ralentisseur.Common.ViewModels;
using ManipulateurFFMpeg.Library;
using System.Configuration;
using Ralentisseur.Common.Services;
using Ralentisseur.Windows.Tests;

namespace Ralentisseur.Windows.ViewModels.Tests
{
    [TestClass]
    public class MainViewModelTests
    {
        #region Properties
        public MainViewModel Vm;
        #endregion

        #region SetUp
        [TestInitialize]
        public void SetUp()
        {
            Vm = new MainViewModel();
        }

        private void SetupValidSource(Mock<IDialogService> dlgMock, IDialogService dlg, string source)
        {
            dlgMock.Setup(d => d.AskSource())
                .Returns(true);
            dlgMock.Setup(d => d.Result)
                .Returns(source);
            Vm.ChooseSource(dlg);
        }

        private void SetupValidDestination(Mock<IDialogService> dlgMock, IDialogService dlg, string destDir)
        {
            dlgMock.Setup(d => d.AskDestination(It.IsAny<string>()))
                           .Returns(true);
            dlgMock.Setup(d => d.Result)
                .Returns(destDir);
            Vm.ChooseDestination(dlg);
        }
        #endregion

        #region Ctor
        [TestMethod]
        public void Ctor()
        {
            //Arrange

            //Act

            //Assert
            Assert.IsNotNull(Vm);
            Assert.IsNotNull(Vm.MainManipulateur);
        }
        #endregion

        #region User Settings Test
        [TestMethod]
        public void UserSettings_WhenDestinationDirectoryChosenButSame_ThenNothing()
        {
            //Arrange
            Mock<IDialogService> dlgMock = new Mock<IDialogService>();
            IDialogService dlg = dlgMock.Object;
            string configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;
            DateTime configChange = File.GetLastWriteTime(configFile);

            //Act
            SetupValidDestination(dlgMock, dlg, Ressources.DestDir);

            //Assert
            Assert.AreEqual(Ressources.DestDir, Properties.Settings.Default.DestinationDirectory);
            Assert.IsFalse(configChange < File.GetLastWriteTime(configFile));
        }

        [TestMethod]
        public void UserSettings_WhenDestinationDirectoryChosen_ThenUpdateSettings()
        {
            //Arrange
            Mock<IDialogService> dlgMock = new Mock<IDialogService>();
            IDialogService dlg = dlgMock.Object;
            string configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;
            string expectedDir = Ressources.SameOutputDir;
            DateTime configChange = File.GetLastWriteTime(configFile);

            //Act
            SetupValidDestination(dlgMock, dlg, Ressources.DestDir);
            SetupValidDestination(dlgMock, dlg, expectedDir);

            //Assert
            Assert.AreEqual(expectedDir, Properties.Settings.Default.DestinationDirectory);
            Assert.IsTrue(configChange < File.GetLastWriteTime(configFile));
        }

        [TestMethod]
        public void UserSettings_WhenAppStarts_ThenLoadSettings()
        {
            //Arrange
            Mock<IDialogService> dlgMock = new Mock<IDialogService>();
            IDialogService dlg = dlgMock.Object;
            SetupValidDestination(dlgMock, dlg, Ressources.DestDir);

            //Act
            var expectedVm = new MainViewModel();

            //Assert
            Assert.AreEqual(Ressources.DestDir, expectedVm.MainManipulateur.DestinationDirectory);
        }
        #endregion
    }
}
