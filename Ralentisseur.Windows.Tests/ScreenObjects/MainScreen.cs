﻿using Ralentisseur.Windows.Tests.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace Ralentisseur.Windows.Tests.ScreenObjects
{
    class MainScreen : AppScreen
    {
        #region private Properties
        private Window _window;
        #endregion

        #region Public Properties
        public virtual Button ChooseDestinationButton
        {
            get => _chooseDestinationButton;
        }
        public virtual string SourcePath
        {
            get => _sourceTextBlock.Text;
        }

        public virtual string DestinationPath
        {
            get => _destinationTextBlock.Text;
        }

        public virtual Window ModalWindow
        {
            get => _window.ModalWindows().Last();
        }
        #endregion

        #region Screen Objects Mapping
        [AutomationId("SourceTextBlock")]
        private readonly Label _sourceTextBlock = null;

        [AutomationId("DestinationTextBlock")]
        private readonly Label _destinationTextBlock = null;

        [AutomationId("ChooseSourceButton")]
        private readonly Button _chooseSourceButton = null;

        [AutomationId("ChooseDestinationButton")]
        private readonly Button _chooseDestinationButton = null;

        [AutomationId("GOButton")]
        private readonly Button _gOButton = null;
        #endregion

        #region Ctor
        public MainScreen(Window window, ScreenRepository screenRepository)
            : base(window, screenRepository)
        {
            this._window = window;
        }
        #endregion

        #region User Actions
        public virtual void ChooseSource(string source)
        {
            _chooseSourceButton.Click();
            var openFileWindow = new OpenFileWindowWrapper(_window.ModalWindows().Last());

            openFileWindow.FilePaths.EditableText = source;
            openFileWindow.OkButton.Click();

            //GOButton.Click();
        }

        public virtual void ChooseDestination(string destDir)
        {
            _chooseDestinationButton.Click();
            var openDirWindow = new CommonOpenFileWindowWrapper(_window.ModalWindows().Last());

            openDirWindow.FilePaths.Text = destDir;

            openDirWindow.OkButton.Click();
        }

        public virtual void Go()
        {
            _gOButton.Click();
        }
        #endregion

        #region Public Test Methods
        public virtual Window GetMessageBox(string title)
        {
            return _window.MessageBox(title);
        }
        #endregion
    }
}
