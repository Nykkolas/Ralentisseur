﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestStack.White;

namespace Ralentisseur.Windows.Tests.ScreenObjects
{
    public abstract class BaseUITest : IDisposable
    {
        public Application Application { get; private set; }

        protected BaseUITest()
        {
            string applicationPath = Path.Combine(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                "Ralentisseur.Windows.exe");

            Application = Application.Launch(applicationPath);
        }

        public void Dispose()
        {
            Application.Dispose();
        }
    }
}
