﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using TestStack.White.Factory;
using System.Reflection;
using Ralentisseur.Windows.Tests.ScreenObjects;
using Ralentisseur.Windows.Tests.Wrappers;
using TestStack.White.ScreenObjects.Sessions;
using TestStack.White.Configuration;
using TestStack.White.ScreenObjects.Services;
using TestStack.White.UIItems.WindowItems;

namespace Ralentisseur.Windows.Tests
{
    /// <summary>
    /// Filetre Explorateur de test : FullName:"Ralentisseur.Windows.Tests.UITests"
    /// </summary>
    [TestClass]
    public class UITests : BaseUITest
    {
        public UITests()
        {
            //
            // TODO: ajoutez ici la logique du constructeur
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active, ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        //
        // Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        // Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test de la classe
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Utilisez ClassCleanup pour exécuter du code une fois que tous les tests d'une classe ont été exécutés
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test 
        [TestInitialize()]
        public void LaunchApplication()
        {
            var workPath = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath);
            var workConfiguration = new WorkConfiguration
            {
                ArchiveLocation = workPath,
                Name = "Ralentisseur"
            };

            CoreAppXmlConfiguration.Instance.WorkSessionLocation = new DirectoryInfo(workPath);
            var workSession = new WorkSession(workConfiguration, new NullWorkEnvironment());
            var screenRepository = workSession.Attach(Application);

            _mainScreen = screenRepository.Get<MainScreen>("Ralentisseur", InitializeOption.NoCache);
        }

        // Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        #region Private Properties
        private MainScreen _mainScreen;
        #endregion

        #region Private Methods
        private bool AreSamePaths(string firstPath, string secondPath)
        {
            return String.Equals(firstPath, secondPath, StringComparison.OrdinalIgnoreCase); //Comparaison insensible à la case car sinon le répertoire est source/repos au lieu de Source/Repo
        }
        #endregion

        #region Given Methods
        private static string DestinationFileDoesntExists(string sourceFile, string destDir)
        {
            string destFile = Path.Combine(destDir, Path.GetFileName(sourceFile));
            if (File.Exists(destFile))
                File.Delete(destFile);
            return destFile;
        }

        private string AValidDestinationDirectoryIsChosen(string destDir)
        {
            _mainScreen.ChooseDestination(destDir);
            return destDir;
        }

        private string AnExistingSourceFileIsChosen(string source)
        {
            _mainScreen.ChooseSource(source);
            return source;
        }
        #endregion

        #region When Methods
        private CommonOpenFileWindowWrapper UserClickOnChooseDestination()
        {
            _mainScreen.ChooseDestinationButton.Click();
            return new CommonOpenFileWindowWrapper(_mainScreen.ModalWindow);
        }
        #endregion

        #region Then Methods
        private void OngoingWindowAppears()
        {
            var ongoingWindow = _mainScreen.ModalWindow;
            Assert.IsNotNull(ongoingWindow);
            Assert.AreEqual("OngoingWindow", ongoingWindow.Title);
        }

        private void DestinationFileIsDisplayedInDestinationLabel(string destFile)
        {
            Assert.IsTrue(AreSamePaths(destFile, _mainScreen.DestinationPath)); 
        }

        private void SourceFileIsDisplayedInSourceLabel(string sourceFile)
        {
            Assert.IsTrue(AreSamePaths(sourceFile, _mainScreen.SourcePath));
        }

        private void DestDirIsStill (string destDir, CommonOpenFileWindowWrapper inDestinationWindow)
        {
            string addressBarName = inDestinationWindow.AddressBar.Name;
            string destWindowPath = addressBarName.Substring(addressBarName.IndexOf(':') + 2);
            Assert.IsTrue(AreSamePaths(destWindowPath, destDir));
        }
        #endregion

        #region Scenarios
        [TestMethod]
        public void Scenario_GivenEmptySource_ThenErrorMessage()
        {
            //Given

            //When
            _mainScreen.Go();

            //Then
            var messageBox = _mainScreen.GetMessageBox("La source n'existe pas");
            Assert.IsNotNull(messageBox);
        }

        [TestMethod]
        public void Scenario_UC1()
        {
            //Given
            string sourceFile = AnExistingSourceFileIsChosen(Ressources.UneSecondeFile);
            string destDir = AValidDestinationDirectoryIsChosen(Ressources.DestDir);
            string destFile = DestinationFileDoesntExists(sourceFile, destDir);

            //When
            _mainScreen.Go();

            //Then
            SourceFileIsDisplayedInSourceLabel(sourceFile);
            DestinationFileIsDisplayedInDestinationLabel(destFile);
            OngoingWindowAppears();
        }

        [TestMethod]
        public void Scenario_UC1SV2a()
        {
            //Given
            string destDir = AValidDestinationDirectoryIsChosen(Ressources.DestDir);
            string sourceFile = AnExistingSourceFileIsChosen(Ressources.UneSecondeFile);
            string anotherDestDir = Ressources.SameOutputDir;

            //When
            var destinationWindow = UserClickOnChooseDestination();

            //Then
            DestDirIsStill(destDir, destinationWindow);
        }
        #endregion
    }
}
