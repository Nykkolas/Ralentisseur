﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WindowStripControls;

namespace Ralentisseur.Windows.Tests.Wrappers
{
    public class CommonOpenFileWindowWrapper
    {
        private readonly Window _window;

        public CommonOpenFileWindowWrapper(Window window)
        {
            _window = window;
            LoadControls();
        }

        public Button CancelButton { get; private set; }
        public Button OkButton { get; private set; }
        public TextBox FilePaths { get; private set; }
        public ComboBox FileTypeFilter { get; private set; }
        public ToolStrip AddressBar { get; private set; }

        #region Overwrite all the TestStack.White.UIItems.WindowItems.Window virtual functions

        // For Example
        //public override string Title { get { return _window.Title; } }

        #endregion

        private void LoadControls()
        {
            CancelButton = _window.Get<Button>(SearchCriteria.ByClassName("Button").AndAutomationId("2"));
            OkButton = _window.Get<Button>(SearchCriteria.ByClassName("Button").AndAutomationId("1"));
            FilePaths = _window.Get<TextBox>(SearchCriteria.ByAutomationId("1152"));
            //FileTypeFilter = _window.Get<ComboBox>(SearchCriteria.ByAutomationId("1136"));
            AddressBar = _window.Get<ToolStrip>(SearchCriteria.ByClassName("ToolbarWindow32").AndAutomationId("1001"));
        }

    }
}

